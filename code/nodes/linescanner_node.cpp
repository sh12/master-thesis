// Standard

// Local
#include <utils.h>
#include <linescanner.h>

int
main(int argc,
     char *argv[])
{
    // Initialize ROS
    ros::init(argc, argv, "linescanner_node");
    ros::NodeHandle nh;

    // Run linescanner node
    Linescanner ls(nh);
    ls.run();

    return 0;
}
