// Standard
#include <iostream>

#include <ros/ros.h>

// Local
#include <utils.h>
#include <tracker.h>

int
main(int argc,
     char *argv[])
{
    // Initialize ROS
    ros::init(argc, argv, "tracker_node");
    ros::NodeHandle nh;

    // Run linescanner node
    Tracker tracker(nh);
    tracker.run();

    return 0;
}
