// Standard

// Local
#include <graph.h>

int
main(int argc,
     char *argv[])
{
//    C c(Point2d(101.807,-7.27362),0.947893);
    C c(Point2d(10.533479, 1.439498), 1.084833);
    Line2d l(Point2d(7.130006, -0.152585), Point2d(9.683092, -1.440199));
    std::cout << c.intersects(l) << std::endl;

    return 0;
}
