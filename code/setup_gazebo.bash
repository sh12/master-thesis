#!/bin/bash

source $PX4DIR/Tools/setup_gazebo.bash $PX4DIR $PX4DIR/build/px4_sitl_default

export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$PX4DIR:$PX4DIR/Tools/sitl_gazebo:$WSDIR/src/mt/models
