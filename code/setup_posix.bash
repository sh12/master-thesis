#!/bin/bash
DIR=$WSDIR/src/mt
find $PX4DIR -lname "$DIR/*" -delete

ln -vs $DIR/init.d-posix/* $PX4DIR/ROMFS/px4fmu_common/init.d-posix/airframes/
#ln -vs $DIR/init.d/* $PX4DIR/ROMFS/px4fmu_common/init.d/airframes/
ln -vs $DIR/mixers/* $PX4DIR/ROMFS/px4fmu_common/mixers/
ln -vs $DIR/models/* $PX4DIR/Tools/sitl_gazebo/models/
ln -vs $DIR/worlds/* $PX4DIR/Tools/sitl_gazebo/worlds/