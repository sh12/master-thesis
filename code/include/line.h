#ifndef LINE_H
#define LINE_H

// Standard includes

// ROS includes

// Local includes
#include <point.h>

// Defines

// Forward declarations


/* Abstract Base Class */
template <class P>
class Line {
public:
    // Constructors/destructors
    explicit Line();
    explicit Line(const P &p,
                  const P &q);
    virtual ~Line() {}

    // Properties
    P &p() {return _p;}
    const P &p() const {return _p;}
    P &q() {return _q;}
    const P &q() const {return _q;}
    double length() const {return _p.dist_to(_q);}

    // Functions
    virtual P project(const P &p) const = 0;
    virtual float dist_to(const P &p) const = 0;

protected:
    P _p, _q;
};


/* Line2d */
class Line2d : public virtual Line<Point2d> {
    typedef Point2d P;
public:
    // Constructors
    explicit Line2d();
    explicit Line2d(const P &p,
                    const P &q);

    // Functions
    P project(const P &p) const override;
    float dist_to(const P &p) const override;

private:
};


/* Line3d */
class Line3d : public virtual Line<Point3d> {
    typedef Point3d P;
public:
    // Constructors
    explicit Line3d();
    explicit Line3d(const P &p,
                    const P &q);

    // Functions
    P project(const P &p) const override {throw std::logic_error("Not implemented!");}
    float dist_to(const P &p) const override {throw std::logic_error("Not implemented!");}

private:
};

#endif
