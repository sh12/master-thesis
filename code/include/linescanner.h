#ifndef LINESCANNER_H
#define LINESCANNER_H

// Standard includes

// ROS includes
#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PoseStamped.h>

// Local includes
#include <mt/Points.h>
#include <utils.h>
#include <circle.h>
#include <sloam.h>

// Defines

// Forward declarations

class Linescanner {
public:
    // Types
    typedef Point2d P;

    // Constructors
    explicit Linescanner(ros::NodeHandle &nh);

    // Properties

    // Functions
    void run();

private:
    // Functions
    void ls2worldxys(const sensor_msgs::LaserScan &ls,
                std::vector<P> &xys) const;
    void worldxys2xys(const std::vector<P> &xys,
                        std::vector<P> &localxys) const ;

    // ROS node
    ros::NodeHandle &_nh;
    const ros::Subscriber _scanSub, _poseSub;
    const ros::Publisher _rvizPub, _pointsPub, _posePub;

    // Messages
    sensor_msgs::LaserScan _currentScan;
    Pose _wTc;

    // Callbacks
    void _scanCb(const sensor_msgs::LaserScan::ConstPtr &msg) {_currentScan = *msg; _newScanReceived = true;}
    void _poseCb(const geometry_msgs::PoseStamped::ConstPtr &msg) {_wTc = *msg;}

    // Variables
    bool _newScanReceived;
};

#endif // LINESCANNER_H
