#ifndef POINT_H
#define POINT_H

// Standard includes
#include <pcl/point_types.h>

// ROS includes
#include <visualization_msgs/MarkerArray.h>

// Local includes
#include <mt/Point.h>
#include <utils.h>
#include <pose.h>

// Defines

// Forward declarations
class Point3d;

/* Point2d */
class Point2d : public Eigen::Vector2d {
protected:
    typedef Point2d P;

public:
    // Constructors/destructors
    explicit Point2d();
    explicit Point2d(const double &x,
                     const double &y);
    Point2d(const Pose &pose);
    Point2d(const Point3d &p);
    Point2d(const Eigen::Vector2d &p);
    Point2d(const mt::Point &p);

    // Properties
    double &x() {return data()[0];}
    const double &x() const {return data()[0];}
    double &y() {return data()[1];}
    const double &y() const {return data()[1];}

    // Functions
    double dist_to(const P &p) const;
    visualization_msgs::Marker toMarker(const uint16_t &id,
                                        const float &duration = 0.0,
                                        const float &r = 1.0,
                                        const float &g = 1.0,
                                        const float &b = 0.0,
                                        const float &a = 1.0) const;
    bool operator==(const P &p) const {return x() == p.x() && y() == p.y();}

    static visualization_msgs::MarkerArray points2markers(const std::vector<P> &points, const float& duration, const float &r = 1.0, const float &g = 1.0, const float &b = 0.0, const float &a = 1.0);
};


/* Point3d */
class Point3d : public Eigen::Vector3d {
protected:
    typedef Point3d P;

public:
    // Constructors/destructors
    explicit Point3d();
    explicit Point3d(const double &x,
                     const double &y,
                     const double &z);
    Point3d(const Pose &pose);
    Point3d(const Eigen::Vector3d &p);
    Point3d(const Point2d &p);
    Point3d(const geometry_msgs::Point &p);

    // Properties
    double &x() {return data()[0];}
    const double &x() const {return data()[0];}
    double &y() {return data()[1];}
    const double &y() const {return data()[1];}
    double &z() {return data()[2];}
    const double &z() const {return data()[2];}

    // Functions
    double dist_to(const Point3d &p) const;
    visualization_msgs::Marker toMarker(const uint16_t &id,
                                        const float &duration = 0.0,
                                        const float &r = 1.0,
                                        const float &g = 1.0,
                                        const float &b = 0.0,
                                        const float &a = 1.0) const;
};

#endif
