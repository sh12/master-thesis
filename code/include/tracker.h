#ifndef TRACKER_H
#define TRACKER_H

// Standard includes

// ROS includes
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

// Local includes
#include <utils.h>
#include <pose.h>

// Defines

// Forward declarations


class Tracker
{
public:
    // Types

    // Constructors
    explicit Tracker(ros::NodeHandle &nh);

    // Properties

    // Functions
    void run();

private:
    // Functions

    // ROS node
    ros::NodeHandle &_nh;
    const ros::Subscriber _poseSub;
    const ros::Publisher _rvizPub;

    // Messages
    Pose _currentPose;

    // Callbacks
    void _poseCb(const geometry_msgs::PoseStamped::ConstPtr &msg) {_currentPose = *msg;}

    // Variables
};

#endif // TRACKER_H
