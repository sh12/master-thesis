#ifndef CIRCLE_H
#define CIRCLE_H

// Standard includes

// ROS includes
#include <visualization_msgs/Marker.h>

// Local includes
#include <mt/Circle.h>
#include <utils.h>
#include <line.h>

// Defines

// Forward declarations

class Circle {
public:
    // Types
    typedef Point2d P;

    // Constructors
    explicit Circle();
    explicit Circle(const P &center, const double &radius);
    explicit Circle(const P &p1, const P &p2, const P &p3);
    explicit Circle(const std::vector<P> &points);
    explicit Circle(const C &c);
    Circle(const mt::Circle &c);

    // Properties
    P &center() {return _center;}
    const P &center() const {return _center;}
    double &radius() {return _radius;}
    const double &radius() const {return _radius;}
    double diameter() const {return 2 * _radius;}

    // Operators
    C &operator=(const C&) = default;
    bool operator==(Circle c) {return _center == c.center() && _radius == c.radius();}

    // Functions
    double dist_to(const P &p) const;
    double dist_to(const Line2d &l) const;
    visualization_msgs::Marker toMarker(const uint16_t &id,
                                        const float &duration = 0.0,
                                        const float &r = 1.0,
                                        const float &g = 1.0,
                                        const float &b = 0.0,
                                        const float &a = 0.5) const;
    std::pair<std::unique_ptr<Circle::P>, std::unique_ptr<Circle::P>> intersection(const Line2d &l) const;
    bool intersects(const Line2d &l) const;

private:
    // Functions

    // Variables
    P _center;
    double _radius;
};

#endif // CIRCLE_H
