#ifndef CERES_OPTIMIZER_H
#define CERES_OPTIMIZER_H

// Standard includes
#include <ceres/ceres.h>
#include <ceres/rotation.h>

// ROS includes
#include <visualization_msgs/MarkerArray.h>

// Local includes
#include <utils.h>

// Defines

// Forward declarations
class Pose;
class Point2d;
struct Landmark;

class CeresOptimizer {
public:
    // Types

    // Constructors
    explicit CeresOptimizer(ros::NodeHandle &nh);

    // Properties

    // Functions
    Eigen::Affine2d optimize_pose(const std::map<Landmark* const, std::vector<Point2d>> &associations,
                                  const float &max_time);

private:
    // Functions

    // ROS
    ros::NodeHandle &_nh;
    const ros::Publisher _optimizerPub;

    // Variables
    std::vector<int8_t> calibDataVec;
    ceres::Solver::Options _options;
    ceres::Solver::Summary _summary;
};

#endif // CERES_OPTIMIZER_H
