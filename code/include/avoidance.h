#ifndef AVOIDANCE_H
#define AVOIDANCE_H

// Standard includes

// ROS includes
#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

// Local includes
#include <mt/Circles.h>
#include <mt/Points.h>
#include <sloam.h>
#include <graph.h>

// Defines

// Forward declarations

class Avoidance {
    // Follows https://expert.taylors.edu.my/file/rems/publication/109291_8486_1.pdf
public:
    // Types
    typedef SLOAM::P P;

    // Constructors
    explicit Avoidance(ros::NodeHandle &nh);

    // Properties
    void set_target(const P &target);
    const P &target() const {return _target;}
    const geometry_msgs::Quaternion &quat() const {return _quat;}
    bool point_reached() const {return _target.dist_to(P(_currentPos)) < OPTIONS::MISSION::DIST_VISITED;}

    // Functions
    Avoidance::P process();
    void print_errors() {
//        std::cout << "\nerrors = [";
//        for (std::pair<ros::Duration, float> err : _errors)
//            std::cout << "[" << err.first << ", " << err.second << "],";
//        std::cout << "]" << std::endl;
    }

private:
    // Functions
    Avoidance::P get_local_target(const P &p) const;

    // ROS node
    ros::NodeHandle &_nh;
    const ros::Subscriber _obstaclesSub, _pointsSub, _posSub;
    const ros::Publisher _closestObstaclesPub;

    // Messages
    mt::Circles _currentObstacles;
    mt::Points _currentPoints;
    geometry_msgs::PoseStamped _currentPos;

    // Callbacks
    void _obstaclesCb(const mt::Circles::ConstPtr &msg) {_currentObstacles = *msg;}
    void _pointsCb(const mt::Points::ConstPtr &msg) {_currentPoints = *msg;}
    void _posCb(const geometry_msgs::PoseStamped::ConstPtr &msg) {_currentPos = *msg;}

    // Variables
    P _origin, _target;
    geometry_msgs::Quaternion _quat;
    std::vector<P> _path;
    RRT _rrt;
    bool _path_found = true;

    // Testing
    const ros::Time _start;
    std::vector<std::pair<ros::Duration, float>> _errors;
};

#endif // AVOIDANCE_H
