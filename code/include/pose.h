#ifndef POSE_H
#define POSE_H

// Standard includes

// ROS includes
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>

// Local includes
#include <utils.h>

// Defines

// Forward declarations
class Point3d;

class Pose {
public:
    // Types
    typedef Point3d P;
    typedef Eigen::Quaterniond Q;

    // Constructors
    explicit Pose();
    explicit Pose(const double &x, const double &y, const double &z, const double &q0, const double &q1, const double &q2, const double &q3);
    Pose(const geometry_msgs::PoseStamped &pose);
    Pose(const Eigen::Affine3d &affine);
    Pose(const Eigen::Affine2d &affine, const float &z = 0);

    // Properties
    const P p() const;
    const Eigen::Affine3d affine() const;
    double &x() {return _x;}
    const double &x() const {return _x;}
    double &y() {return _y;}
    const double &y() const {return _y;}
    double &z() {return _z;}
    const double &z() const {return _z;}
    Q &q() {return _q;}
    const Q &q() const {return _q;}
    double &q0() {return _q.w();}
    const double &q0() const {return _q.w();}
    double &q1() {return _q.x();}
    const double &q1() const {return _q.x();}
    double &q2() {return _q.y();}
    const double &q2() const {return _q.y();}
    double &q3() {return _q.z();}
    const double &q3() const {return _q.z();}

    // Functions
    P T(const P &p) const;
    P Tinv(const P &p) const;
    geometry_msgs::PoseStamped toMarker() const;
    static visualization_msgs::Marker create_line(const std::string &ns,
                                                  const float &r = 1.0,
                                                  const float &g = 1.0,
                                                  const float &b = 1.0,
                                                  const float &a = 0.5);
    void update_line(visualization_msgs::Marker &line) const;
    void to2d(Eigen::Affine2d &aff) const;
    void from2d(const Eigen::Affine2d &aff);
    bool operator==(const Pose &pose) const;
    Pose operator*(const Pose &pose) const {return affine() * pose.affine();}
    P operator*(const P &p) const;

private:
    // Functions

    // Variables
    double _x, _y, _z;
    Q _q;
};

#endif // POSE_H
