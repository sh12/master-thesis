#ifndef SLOAM_H
#define SLOAM_H

// Standard includes
#include <pcl/search/kdtree.h>

// ROS includes
#include <ros/ros.h>
#include <mt/Circles.h>

// Local includes
#include <utils.h>
#include <circle.h>
#include <ceres_optimizer.h>

// Defines
struct Landmark {
    typedef Point2d P;
    std::vector<P> points;
    C circle;

    Landmark() = default;
    Landmark(const Landmark &lm)
        : points{lm.points}, circle{lm.circle} {}
};

// Forward declarations

class SLOAM {
    /* From https://arxiv.org/pdf/1912.12726.pdf
     * Indices:
     * k            # Sweep index
     * i            # Landmark index
     * j            # Point index

     * Sizes:
     * Nk           # Size of LLk (number of landmarks for sweep k)
     * dik          # Size of TTki (number of feature points for landmark i in sweep k)
     * gamma        # Size of GGk (number of ground feature points in sweep k)
     * K            # Number of sweeps

     * Defines:
     * PP           # Sweep
     * t            # Current timestamp (tk)
     * LL           # Landmark
     * TT           # Feature points
     * p            # Individual feature point
     * GG           # Ground feature points
     * T            # Pose transform
     * txyz         # Translations along x, y, and z axis
     * thetaxyz     # Right-hand rotations about x, y, and z axis

     * Usages:
     * PPk          # Lidar sweep k
     * tk           # Timestamp for sweep k
     * LLk          # Set of landmarks in sweep k
     * LLki         # Individual landmark i in sweep k
     * TTki         # Set of feature points for landmark i in sweep k
     * TTk          # Feature points for all landmarks in sweep k
     * GGk          # Ground feature points in sweep k

     * Projections to lidar frame:
     * LLkbar       # Projection of landmarks in sweep k
     * TTkbar       # Projection of feature points for all landmarks in sweep k
     * GGkbar       # Projection of ground feature points in sweep k
     * PPkbar       # Projection of lidar sweep k

     * Transforms
     * Tkp1L        # Lidar pose transform between tk and tk+1
     * Tkp1W        # Tkp1L defined in world frame
     * XX           # Sensor state trajectory based on Tkp1W
     * */

public:
    // Types
    typedef Landmark::P P;
    typedef pcl::PointXYZ pclP;
    typedef pcl::PointCloud<pclP> pclC;

    // Constructors
    explicit SLOAM(ros::NodeHandle &nh, const Eigen::Affine2d &wTc);

    // Properties

    // Functions
    void process(std::vector<P> &scan, const Eigen::Affine2d &pTc, const bool &sweep_finish);

private:
    // Functions

    // ROS node
    ros::NodeHandle &_nh;
    const ros::Publisher _posePub, _scanAreaPub, _linePub, _landmarksPub, _landmarksPrevPub, _landmarksWorldCurPub, _landmarksWorldPub, _associationsPub, _circlesPub;

    // Messages
    visualization_msgs::Marker _line;

    // Callbacks

    // Variables
    bool _sweep_start = true;
    Eigen::Affine2d _wTc;
    CeresOptimizer _ceres;
    std::vector<std::unique_ptr<Landmark>> _landmarks_world, _landmarks_prev, _landmarks_cur;
};

#endif // SLOAM_H
