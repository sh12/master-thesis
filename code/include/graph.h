#ifndef GRAPH_H
#define GRAPH_H

#include <utils.h>

// Standard includes
#include <pcl/search/kdtree.h>

// Local includes
#include <line.h>
#include <circle.h>

// Defines

// Forward declarations

class RRT {
public:
    // Types
    typedef Point2d P;
    typedef pcl::PointXY pclP;
    typedef pcl::PointCloud<pclP> pclC;

    class Node {
    public:
        // Types

        // Constructors
        explicit Node(const P &p, Node *parent = nullptr);

        // Properties
        Node *parent() {return _parent;}
        const Node *parent() const {return _parent;}
        const std::vector<std::unique_ptr<Node>> &children() const {return _children;}
        bool is_leaf() const {return _children.empty();}
        std::vector<Node*> leaves() const;
        const P &p() const {return _p;}
        P &p() {return _p;}
        float cost() const;
        uint16_t level() const;
        size_t size() const;

        // Functions
        RRT::Node* add_child(const P &child);
        Node *find(const P &p) const;
        void print() const;
        void path_from_root(std::vector<RRT::P> &path) const;
        void give_child(Node *child, Node* parent);

    private:
        // Functions
        void get_leaves(std::vector<Node*> &leaves) const;
        void get_cost(float &cost) const;
        void get_level(uint16_t &level) const;
        void get_size(size_t &size) const;
        void get_path_to_root(std::vector<P> &path,
                              uint16_t &level) const;
        void do_print(const uint8_t &rel_level,
                      const uint8_t &diff) const;

        // Variables
        Node *_parent;
        std::vector<std::unique_ptr<Node>> _children;
        P _p;
    };

    // Constructors
    explicit RRT();

    // Properties
    uint8_t stuck_counter() const {return ceil(_stuck_multiplier / (OPTIONS::MISSION::UPDATE_RATE * OPTIONS::AVOIDANCE::STUCK_TIME));}

    // Functions
    bool search(const P &start,
                const P &target,
                std::vector<C> obstacles,
                std::vector<P> &path);

private:
    // Types

    // Functions
    void set_problem(const P &start,
                     const P &target);
    P generate_random_point() const;
    bool rrtstar(const P &p,
                 std::vector<C> &obstacles);
    void cloud_add_node(Node * const n);
    void reset();

    // Variables
    std::unique_ptr<Node> _root;
    P _target;
    pcl::search::KdTree<pclP> _kdtree{true};
    pclC::Ptr _cloud{new pclC};
    std::vector<Node*> _node_lookup;
    uint8_t _stuck_multiplier;
};

#endif // GRAPH_H
