#ifndef MISSIONSTEP_H
#define MISSIONSTEP_H

// Standard includes

// ROS includes

// Local includes
#include <point.h>

// Defines

// Forward declarations
class MissionPoint;
class MissionSequence;


/* Abstract Base Class */
class MissionStep {
protected:
    // Types
    typedef MissionStep MSt;
    typedef MissionPoint MP;
    typedef MissionSequence MS;
    typedef Point3d P;

    // Constructors
    explicit MissionStep(const float &max_duration);

public:
    // Types
    enum RunStatus {
        InProgress,
        Finished,
        OutOfTime
    };

    // Destructors
    virtual ~MissionStep() {}

    // Properties
    ros::Time &deadline() {return _deadline;}
    const ros::Time &deadline() const {return _deadline;}
    const ros::Duration &max_duration() const {return _max_duration;}
    ros::Duration remaining_duration() const {return _deadline - ros::Time::now();}

    // Functions
    virtual std::tuple<Point3d, RunStatus> run_step(const Pose &pose, bool &force_next_point) = 0;
    virtual void print(const uint8_t hierarchy = 0) const = 0;
    virtual void reset() = 0;

private:
protected:
    ros::Time _begin, _deadline;
    ros::Duration _max_duration;
};


/* MissionPoint */
class MissionPoint : public virtual MissionStep {
public:
    // Constructors
    explicit MissionPoint(const P &point,
                          const float &max_duration = 0);
    explicit MissionPoint(const Point2d &point,
                          const float &z = 0,
                          const float &max_duration = 0);
    explicit MissionPoint(const double &x,
                          const double &y,
                          const double &z,
                          const float &max_duration = 0);
    explicit MissionPoint(const MissionPoint &mp);

    // Properties
    double &x() {return _p.x();}
    const double &x() const {return _p.x();}
    double &y() {return _p.y();}
    const double &y() const {return _p.y();}
    double &z() {return _p.z();}
    const double &z() const {return _p.z();}
    const P &p() const {return _p;}

    // Overridden functions
    std::tuple<Point3d, RunStatus> run_step(const Pose &pose, bool &force_next_point) override;
    void print(const uint8_t hierarchy = 0) const override;
    void reset() override;

    // Functions
    double dist_to(const P &point) const;
    double dist_to(const MP &point) const;

private:
    P _p;
};


/* MissionSequence */
class MissionSequence : public virtual MissionStep {
protected:
    std::vector<std::unique_ptr<MSt>> _steps;

public:
    // Constructors
    explicit MissionSequence(const float &max_duration = 0,
                             const uint8_t &num_rounds = 1);

    // Properties (vector forwards)
    MSt *front() {return _steps.front().get();}
    const MSt *front() const {return _steps.front().get();}
    MSt *back() {return _steps.back().get();}
    const MSt *back() const {return _steps.back().get();}
    bool empty() const {return _steps.empty();}
    // Properties
    const uint8_t &cur_round() const {return _cur_round;}
    uint8_t &num_rounds() {return _num_rounds;}
    const uint8_t &num_rounds() const {return _num_rounds;}
    MP *first_point();
    const MP *first_point() const;
    MP *last_point();
    const MP *last_point() const;
    void points(std::vector<P> &points) const;
    float distance() const;

    // Overridden functions
    std::tuple<Point3d, RunStatus> run_step(const Pose &pose, bool &force_next_point) override;
    void print(const uint8_t hierarchy = 0) const override;
    void reset() override;

    // Functions
    void add_step(std::unique_ptr<MSt> step);
    void add_step(const uint16_t position,
                  std::unique_ptr<MSt> step);

private:
    uint16_t _cur_step = 0;
    uint8_t _cur_round = 0, _num_rounds;
};

#endif // MISSIONSTEP_H
