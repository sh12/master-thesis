#ifndef PLANNER_H
#define PLANNER_H

// Standard includes
#include <pcl/kdtree/kdtree.h>

// ROS includes
#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>

// Local includes
#include <mission.h>

// Defines

// Forward declarations

class Planner {
public:
    // Types
    typedef Point2d P;

    // Constructors
    explicit Planner(ros::NodeHandle &nh);

    // Properties

    // Functions
    Mission
    generate_mission(std::vector<P> &boundary,
                     const std::vector<std::vector<P>> &obstacles) const;

private:
    // Functions

    // ROS node
    ros::NodeHandle &_nh;
    const ros::Publisher _boundaryPub, _obstaclesPub, _convexPub, _pathPub;
    const ros::Subscriber _poseSub;

    // Messages
    Pose _currentPose;

    // Callbacks
    void _poseCb(const geometry_msgs::PoseStamped::ConstPtr &msg) {_currentPose = *msg;}

    // Variables
};

#endif // PLANNER_H
