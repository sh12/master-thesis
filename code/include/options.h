#ifndef OPTIONS_H
#define OPTIONS_H

#include <cstdint>
#include <string>
#include <Eigen/Dense>

namespace OPTIONS {
    namespace DRONE {
        const std::string NAME                  = "mt_drone"; //                                    The drone's name in ROS
        const float HITBOX                      = 0.52; // [m]                                      Circular hitbox of drone

        auto _droneTscanner = [](){
            Eigen::Affine3d dTs;
            const float theta = M_PI_2 / 9;
            const float c = cos(theta);
            const float s = sin(theta);
            dTs.linear() << c, 0, s, 0, 1, 0, -s, 0, c; //                                          Set droneTscanner rotation
            dTs.translation() << 0.08, 0, -0.08; //                                                 Set droneTscanner translation
            return dTs;
        };
        const Eigen::Affine3d droneTscanner     = _droneTscanner(); //                              3d affine transformation matrix
    }

    namespace PLANNER {
        const float PATH_SPACING                = 4.0; // [m]                                       Space between planned paths
        const float ALTITUDE                    = 1.3; // [m]                                         Altitude to maintain
    }

    namespace MISSION {
        const float UPDATE_RATE                 = 20.0; // [Hz]                                     Rate at which mission should run (is also avoidance rate)
        const float MAX_VEL                     = 1.0; // [m/s]                                     Maximum horizontal velocity
        const float MAX_YAW_RATE                = 30.0; // [deg/s]                                  Maximum yaw rate
        const float DIST_VISITED                = 2.0; // [m]                                       Consider point visited when closer than this
    }

    namespace TRACKER {
        const float UPDATE_RATE                 = 5.0; // [Hz]                                      Rate at which tracker should update
    }

    namespace LINESCANNER {
        const float UPDATE_RATE                 = 10.0; // [Hz]                                     Rate at which linescans should be used (max 10)
        const float MIN_DIST_THRESH             = 0.35; // [m]                                      Remove points closer than this distance
        const float RANGE                       = 5.5; // [m]                                       Range of linescanner
    }

    namespace SLOAM {
    enum ASSOCIATION_TYPE {
        POINTS, //                                                                                  Use points when associating
        CIRCLE //                                                                                   Use circle periphery when associating
    };

        const uint8_t SWEEP_ITER                = 10; // [#]                                         Number of iterations before sweep finished
        const float ASSOCIATE_DIST              = 0.5; // [m]                                       Max distance for point to be associated with circle
        const ASSOCIATION_TYPE ASSOCIATE_TYPE   = CIRCLE; //                                        See association types above
        const float SEG_CUT_THRESH              = 0.5; // [m]                                       New cluster when distance exceeds this
        const float CIRC_MIN_RADIUS             = 0.05; // [m]                                      If circle has radius less than this, remove it
        const float CIRC_MAX_RADIUS             = 1.0; // [m]                                       If circle has radius larger than this, remove it
        const float CIRC_MAX_OFFSET             = LINESCANNER::RANGE + CIRC_MAX_RADIUS; // [m]      If circle further than this distance away, remove it
        const float CIRC_MAX_VAR                = 0.001; // [m^2]                                   Maximum point-to-periphercy variance of points initially defining a circle
        const uint8_t CIRC_MIN_NUM_PTS          = 5; // [#]                                         If the number of points initially defining a circle is less than this, omit it
        const float OUTLIER_RADIUS              = 0.3; // [m]                                       Outlier removal radius
        const uint8_t MIN_INLIERS               = CIRC_MIN_NUM_PTS - 1; // [#]                      Minimum number of inliers within above radius
    }

    namespace CERES {
        const uint8_t NUM_THREADS               = 4; // [#]                                         Number of threads to run Ceres solver on
        const float MAX_TIME                    = 0.5; // [s]                                       Maximum time to spend on optimising pose
    }

    namespace AVOIDANCE {
        const float DIST_HORISON                = 5.0; // [m]                                       Base local target on horison with this radius
        const float DIST_INFLUENCE              = LINESCANNER::RANGE; // [m]                        Only consider obstacles closer than this distance
        const float DIST_PROXIMITY              = 1.0; // [m]                                       Don't allow the UAV closer to obstacles than this
        const float DIST_VISITED                = SLOAM::CIRC_MAX_RADIUS + DRONE::HITBOX; // [m]    Consider point visited when closer than this
        const float STUCK_TIME                  = 5.0; // [s]                                       If stuck more than this, calculate new route
        const float STUCK_VAR_THRESH            = 0.08; // [m^2]                                    If position variance during STUCK_TIME is below this, consider drone stuck

        namespace RRT {
            const float TIME_RUN_MAX            = 0.8 / MISSION::UPDATE_RATE; // [s]                Maximum time for RRT to run
            const float DIST_PROXIMITY          = 0.1 + DRONE::HITBOX; // [m]                       Disallow RRT to find waypoints closer to obstacles than this
            const float DIST_RRTSTAR            = 3.0; // [m]                                       Allow RRT* to optimise inside this radius
            const float AREA_PADDING            = LINESCANNER::RANGE; // [m]                        Pad this distance to the search area
            const float DIST_MAX                = 0.5; // [m]                                       Limit random sample to this distance from other nodes
            const float DIST_HORISON_STUCK      = 1.0; // [m]                                       Increase horison by this every time rrt stuck counter increases
        }
    }
}

#endif // OPTIONS_H
