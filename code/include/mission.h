#ifndef MISSION_H
#define MISSION_H

// Standard includes

// ROS includes
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/ParamSet.h>

// Local includes
#include <missionstep.h>
#include <avoidance.h>

// Defines
#define MODE_OFFBOARD   "OFFBOARD"
#define MODE_LAND       "AUTO.LAND"
#define MODE_LOITER     "AUTO.LOITER"

// Forward declarations

class Mission : public MissionSequence {
public:
    // Types
    enum Result {
        COMPLETED,
        FAILED
    };

    // Constructors
    explicit Mission(ros::NodeHandle &nh,
                     const bool &relative = true,
                     const float &max_duration = 0);

    // Properties
    const Pose pose() const {return _currentPose;}

    // Functions
    Result run();

private:
    // Functions
    void move_to(const Point3d &point);
    void move_to(const double &x,
                 const double &y,
                 const double &z);
    void set_px4_param(const std::string &id, const float &value);

    // ROS node
    ros::NodeHandle &_nh;
    const ros::Subscriber _stateSub, _poseSub;
    const ros::Publisher _targetPosePub, _targetPub, _targetLocalPub, _targetSLOAMPub;
    ros::ServiceClient _armingClient, _setModeClient, _setParamClient;

    // Messages
    mavros_msgs::State _currentState;
    geometry_msgs::PoseStamped _currentPose;

    // Callbacks
    void _stateCb(const mavros_msgs::State::ConstPtr &msg) {_currentState = *msg;}
    void _poseCb(const geometry_msgs::PoseStamped::ConstPtr &msg) {_currentPose = *msg;}

    // Variables
    Pose _home;
    bool _relative;
    Avoidance _avoidance;
};

#endif
