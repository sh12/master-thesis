#ifndef UTILS_H
#define UTILS_H

// Standard includes
#include <iostream>
#include <cstdint>
#include <cmath>
#include <vector>

// ROS includes
#include <ros/ros.h>

// Local includes
#include <types.h>
#include <options.h>

// Defines

// Forward declarations

double
dist_minkowski(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1,
               const uint8_t order);

double
dist_minkowski(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1,
               const double &z0,
               const double &z1,
               const uint8_t order);


double
dist_manhattan(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1);

double
dist_manhattan(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1,
               const double &z0,
               const double &z1);


double
dist_euclidean(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1);

double
dist_euclidean(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1,
               const double &z0,
               const double &z1);

float
get_angle(const Eigen::Vector2d &a,
          const Eigen::Vector2d &b);


#endif
