#include <ceres_optimizer.h>
#include <sloam.h>

// Private functions
namespace {
class CostFunctor {
public:
    CostFunctor(const Landmark::P &p, const C &c)
        : _p{p}, _c{c} {}

    template<typename T>
    bool operator()(const T* const x,
                    const T* const y,
                    const T* const theta,
                    T *residual) const {
        // Construct affine matrix
        Eigen::Transform<T, 2, Eigen::Affine> affine;
        const T s = ceres::sin(*theta);
        const T c = ceres::cos(*theta);

        affine.linear() << c, -s, s, c;
        affine.translation() << *x, *y;

        // Calculate loss
        residual[0] = (_c.center().cast<T>() - affine * _p.cast<T>()).norm() - _c.radius();
        return true;
    }

private:
    const Landmark::P &_p;
    const C &_c;
};
}

// Constructors
CeresOptimizer::CeresOptimizer(ros::NodeHandle &nh)
    : _nh{nh},
      _optimizerPub{_nh.advertise<visualization_msgs::MarkerArray>("/" + OPTIONS::DRONE::NAME + "/ceres/optim", 1)}
{
    // http://ceres-solver.org/nnls_solving.html#solver-options
    _options.minimizer_type = ceres::LINE_SEARCH;
    _options.linear_solver_type = ceres::DENSE_QR;
    _options.minimizer_progress_to_stdout = false;
    _options.num_threads = OPTIONS::CERES::NUM_THREADS;
}

// Functions
Eigen::Affine2d
CeresOptimizer::optimize_pose(const std::map<Landmark* const, std::vector<Landmark::P>> &associations,
                              const float &max_time)
{
    const ros::Time t_start = ros::Time::now();

    double x = 0;
    double y = 0;
    double theta = 0;

    // Add residuals
    ceres::Problem problem;
    problem.AddParameterBlock(&x, 1);
    problem.AddParameterBlock(&y, 1);
    problem.AddParameterBlock(&theta, 1);
    for (const std::pair<Landmark* const, std::vector<Landmark::P>> &a : associations) {
        for (const Landmark::P &p : a.second)
            problem.AddResidualBlock(new ceres::AutoDiffCostFunction<CostFunctor, 1, 1, 1, 1>(new CostFunctor(p, a.first->circle)),
                                     nullptr, &x, &y, &theta);
    }

    // Solve
    const float remaining_time = OPTIONS::CERES::MAX_TIME * max_time - (ros::Time::now() - t_start).toSec();
    _options.max_solver_time_in_seconds = remaining_time < 0 ? OPTIONS::CERES::MAX_TIME / OPTIONS::LINESCANNER::UPDATE_RATE : remaining_time;
    ceres::Solve(_options, &problem, &_summary);

    // Store results
    Eigen::Affine2d result;
    const double c = cos(theta);
    const double s = sin(theta);
    result.linear() << c, -s, s, c;
    result.translation() << x, y;

//    std::cout << "POSE: " << _summary.BriefReport() << "\n" << _summary.message << std::endl;
//    std::cout << "Final mat:\n" << result.matrix() << std::endl;

    return result;
    if (_summary.termination_type == ceres::TerminationType::CONVERGENCE)
        return result;
    else {
        ROS_ERROR("No convergence! Returning identity transform.");
        return Eigen::Affine2d::Identity();
    }
}
