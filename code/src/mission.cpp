#include <mission.h>

// Private functions
namespace {
visualization_msgs::Marker
point2marker(const Point3d &point,
             const std::string &ns,
             const float &r = 0,
             const float &g = 1,
             const float &b = 0,
             const float &a = 1)
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = ns;
    marker.id = 0;
    marker.type = visualization_msgs::Marker::ARROW;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.orientation.w = 1;

    // Insert from/to points
    marker.points.emplace_back();
    marker.points.emplace_back();
    geometry_msgs::Point &p_start = marker.points.front();
    geometry_msgs::Point &p_end = marker.points[1];

    // Set location
    p_start.x = point.x();
    p_start.y = point.y();
    p_start.z = OPTIONS::PLANNER::ALTITUDE + 1;
    p_end = p_start;
    p_end.z -= 1;

    // Size
    marker.scale.x = 0.1;
    marker.scale.y = 0.2;
    marker.scale.z = 0;

    // Color
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;
    marker.color.a = a;

    marker.lifetime = ros::Duration(0);

    return marker;
}
}


// Constructors
Mission::Mission(ros::NodeHandle &nh,
                 const bool &relative,
                 const float &max_duration)
    : MSt{max_duration}, MS{max_duration}, _nh{nh},
      _stateSub{_nh.subscribe<mavros_msgs::State>("/mavros/state", 1, &Mission::_stateCb, this)},
      _poseSub{_nh.subscribe<geometry_msgs::PoseStamped>("/mavros/local_position/pose", 1, &Mission::_poseCb, this)},
      _targetPosePub{_nh.advertise<geometry_msgs::PoseStamped>("/mavros/setpoint_position/local", 1)},
      _targetPub{_nh.advertise<visualization_msgs::Marker>("/" + OPTIONS::DRONE::NAME + "/target", 1)},
      _targetLocalPub{_nh.advertise<visualization_msgs::Marker>("/" + OPTIONS::DRONE::NAME + "/target_local", 1)},
      _targetSLOAMPub{_nh.advertise<visualization_msgs::Marker>("/" + OPTIONS::DRONE::NAME + "/target_sloam", 1)},
      _armingClient{_nh.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/arming")},
      _setModeClient{_nh.serviceClient<mavros_msgs::SetMode>("/mavros/set_mode")},
      _setParamClient{_nh.serviceClient<mavros_msgs::ParamSet>("/mavros/param/set")},
      _relative{relative},
      _avoidance{_nh} {}


// Functions
Mission::Result
Mission::run()
{
    // Create softer landing
//    const MP *lp = last_point();
//    MP temp(*lp);
//    for (uint8_t i = 0; i < 3; ++i) {
//        temp.z() = lp->z() * (3 - i) / 4;
//        add_step(std::make_unique<MP>(temp));
//    }
//    print();

    ROS_INFO("Running mission (dist=%f)!", distance());
    ros::Rate rate(OPTIONS::MISSION::UPDATE_RATE);

    // Wait for FCU connection
    ROS_INFO("Waiting for connection");
    while (ros::ok() && !_currentState.connected) {
        ros::spinOnce();
        rate.sleep();
    }

    // Wait until pose can be gathered
    ROS_INFO("Waiting for pose");
    do {
        ros::spinOnce();
        _home = pose();
        rate.sleep();
    } while (ros::ok() && _home.x() == 0 && _home.y() == 0 && _home.z() == 0);
    ROS_INFO("Home: %f %f %f", _home.x(), _home.y(), _home.z());

    // Set px4 parameters
    set_px4_param("MPC_XY_VEL_MAX", OPTIONS::MISSION::MAX_VEL);
    set_px4_param("MC_YAWRATE_MAX", OPTIONS::MISSION::MAX_YAW_RATE);

    // Custom modes (http://wiki.ros.org/mavros/CustomModes#PX4_native_flight_stack)
    mavros_msgs::SetMode offb_set_mode;
    offb_set_mode.request.custom_mode = MODE_OFFBOARD;
    mavros_msgs::SetMode land_set_mode;
    land_set_mode.request.custom_mode = MODE_LAND;
    mavros_msgs::SetMode loiter_set_mode;
    loiter_set_mode.request.custom_mode = MODE_LOITER;

    // ARM command
    mavros_msgs::CommandBool arm_cmd;
    arm_cmd.request.value = true;

    // States
    enum STATE {
        EN_OFFBOARD,
        WAIT_FOR_OFFBOARD,
        ARM,
        WAIT_FOR_ARMED,
        EXECUTE,
        WAIT_FOR_LAND,
        LANDING,
        FINISH
    };

    // Check if sim
    bool simulation = false;
    _nh.getParam("sim", simulation);
    ROS_INFO("simulation = %d", simulation);

    if (simulation)
        ROS_WARN("RUNNING GAZEBO");
    else ROS_WARN("RUNNING FOR REAL");
    STATE state = simulation ? EN_OFFBOARD : WAIT_FOR_OFFBOARD;

    // Helper variables
    Point3d point;
    Result result;
    RunStatus status;
    ros::Time last_request = ros::Time::now();

    while (ros::ok()) {
        switch (state) {
        case EN_OFFBOARD:
            ROS_INFO("Enabling offboard");
            if (_relative)
                move_to(0, 0, 0);
            else move_to(_home);
            if (_currentState.mode != MODE_OFFBOARD && _setModeClient.call(offb_set_mode) && offb_set_mode.response.mode_sent)
                state = WAIT_FOR_OFFBOARD;
            last_request = ros::Time::now();
            break;

        case WAIT_FOR_OFFBOARD:
            ROS_INFO("Waiting for offboard");
            if (_relative)
                move_to(0, 0, 0);
            else move_to(_home);
            if (_currentState.mode == MODE_OFFBOARD) {
                ROS_INFO(">> Offboard enabled");
                state = ARM;
            }
            break;

        case ARM:
            if (_relative)
                move_to(0, 0, 0);
            else move_to(_home);
            if (_currentState.mode != MODE_OFFBOARD)
                state = FINISH;
            else if (!_currentState.armed) {
                ROS_INFO("Arming");
                if (_armingClient.call(arm_cmd) && arm_cmd.response.success)
                    state = WAIT_FOR_ARMED;
                last_request = ros::Time::now();
            }
            break;

        case WAIT_FOR_ARMED:
            if (_currentState.armed) {
                ROS_INFO(">> Vehicle armed, executing");
                _avoidance.set_target(first_point()->p());
                state = EXECUTE;
            }
            break;

        case EXECUTE:
            if (_currentState.mode != MODE_OFFBOARD)
                state = FINISH;
            else if (_currentState.armed) {
                // Run whatever step we're at
                bool force_next_point = _avoidance.point_reached() && 1 < _currentPose.pose.position.z;
                std::tie(point, status) = MS::run_step(_currentPose, force_next_point);

                // Handle status
                if (status == InProgress) {
                    move_to(point);
                    _avoidance.print_errors();
                } else {
                    switch (status) {
                    case Finished:
                        ROS_INFO("Finished!");
                        _avoidance.print_errors();
                        result = COMPLETED;
                        break;

                    case OutOfTime:
                        ROS_WARN("Out of time!");
                        result = FAILED;
                        break;

                    default:
                        throw std::invalid_argument("Invalid status: " + std::to_string(status));
                        break;
                    }

                    if (_setModeClient.call(land_set_mode) && land_set_mode.response.mode_sent) {
                        ROS_INFO("Enabling %s", MODE_LAND);
                        state = WAIT_FOR_LAND;
                    }
                }
            }
            break;

        case WAIT_FOR_LAND:
            if (_currentState.mode == MODE_LAND) {
                ROS_INFO(">> AUTO.LAND enabled, waiting until disarmed");
                state = LANDING;
            }
            break;

        case LANDING:
            if (!_currentState.armed) {
                ROS_INFO("Disarmed");
                if (_setModeClient.call(loiter_set_mode) && loiter_set_mode.response.mode_sent) {
                    ROS_INFO("Enabling %s", MODE_LOITER);
                }
                state = FINISH;
            }
            break;

        default:
            throw std::invalid_argument("Invalid state: " + std::to_string(state));
            break;
        }

        ros::spinOnce();
        rate.sleep();

        // Mode + armed mode print
        ROS_DEBUG("mode: %s, armed: %u", _currentState.mode.c_str(), _currentState.armed);

        if (state == FINISH)
            break;
    }

    return result;
}

void
Mission::move_to(const P &point)
{
    // If new target point
    const Avoidance::P point_2d(point);
    if (_avoidance.target() != point_2d) {
        _avoidance.set_target(point_2d);

        // Display helpful stuff
        ROS_INFO("Moving to %f %f %f%s", point.x(), point.y(), point.z(), _relative ? " (relative)" : "");
        if (0 < _targetPub.getNumSubscribers())
            _targetPub.publish(visualization_msgs::Marker(point2marker(point, "mission_target", 0, 1, 0)));
    }

    // Calculate local target through obstacle avoidance
    Avoidance::P target_mavros = point;
    if (1.0 < _currentPose.pose.position.z) {
        const Avoidance::P target_local = _avoidance.process();
        Eigen::Affine2d wTm;
        Pose(_currentPose).to2d(wTm);
        target_mavros = wTm * target_local;
        if (0 < _targetSLOAMPub.getNumSubscribers())
            _targetSLOAMPub.publish(visualization_msgs::Marker(point2marker(target_local, "mission_target_sloam", 1, 0, 0)));
    }

    // Set pose
    geometry_msgs::PoseStamped pose;
    pose.pose.orientation = _avoidance.quat();
    geometry_msgs::Point &pos = pose.pose.position;
    pos.x = target_mavros.x();
    pos.y = target_mavros.y();
    pos.z = OPTIONS::PLANNER::ALTITUDE;
    if (_relative) {
        pos.x += _home.x();
        pos.y += _home.y();
        pos.z += _home.z();
    }

    // Visualise
    if (0 < _targetLocalPub.getNumSubscribers())
        _targetLocalPub.publish(visualization_msgs::Marker(point2marker(target_mavros, "mission_target_local", 1, 1, 1, 0.5)));

    _targetPosePub.publish(pose);
}

void
Mission::move_to(const double &x,
                 const double &y,
                 const double &z)
{
    move_to(P(x, y, z));
}

void
Mission::set_px4_param(const std::string &id,
                       const float &value)
{
    ROS_INFO("PX4 param %s -> %f", id.c_str(), value);

    // Set PX4 params
    mavros_msgs::ParamSet param_set;
    param_set.request.param_id = id.c_str();
//    param_set.request.value.integer = value;
    param_set.request.value.real = value;

    // Wait until success
    while (ros::ok() && !(_setParamClient.call(param_set) && param_set.response.success)) {
        ROS_INFO("Waiting ...");
        ros::spinOnce();
        ros::Duration(1).sleep();
    }
}
