#include <pose.h>
#include <point.h>


// Private functions
namespace {}


// Constructors
Pose::Pose() {}

Pose::Pose(const double &x, const double &y, const double &z,
           const double &q0, const double &q1, const double &q2, const double &q3)
    : _x{x}, _y{x}, _z{z}, _q{q0, q1, q2, q3} {}

Pose::Pose(const geometry_msgs::PoseStamped &pose)
{
    const geometry_msgs::Point &pos = pose.pose.position;
    const geometry_msgs::Quaternion &q = pose.pose.orientation;
    _x = pos.x;
    _y = pos.y;
    _z = pos.z;
    _q = Eigen::Quaterniond(q.w, q.x, q.y, q.z);
}

Pose::Pose(const Eigen::Affine3d &affine)
    : _x{affine.translation()[0]}, _y{affine.translation()[1]}, _z{affine.translation()[2]}, _q{affine.rotation()} {}

Pose::Pose(const Eigen::Affine2d &affine,
           const float &z)
{
    from2d(affine);
    _z = z;
}


// Properties
const Pose::P Pose::p() const
{
    return P(_x, _y, _z);
}

const Eigen::Affine3d Pose::affine() const
{
    Eigen::Affine3d affine;
    affine.linear() = _q.toRotationMatrix();
    affine.translation() = p();
    return affine;
}

// Functions
Pose::P
Pose::T(const P &p) const
{
    return P(_q * p + this->p());
}

Pose::P
Pose::Tinv(const P &p) const
{
    return _q.inverse() * (p - this->p());
}

geometry_msgs::PoseStamped
Pose::toMarker() const
{
    geometry_msgs::PoseStamped marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();

    // Set pose
    marker.pose.position.x = _x;
    marker.pose.position.y = _y;
    marker.pose.position.z = _z;
    marker.pose.orientation.x = _q.x();
    marker.pose.orientation.y = _q.y();
    marker.pose.orientation.z = _q.z();
    marker.pose.orientation.w = _q.w();

    return marker;
}

visualization_msgs::Marker
Pose::create_line(const std::string &ns,
                  const float &r,
                  const float &g,
                  const float &b,
                  const float &a)
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = ns;
    marker.id = 0;
    marker.type = visualization_msgs::Marker::LINE_STRIP;
    marker.action = visualization_msgs::Marker::ADD;

    // Position
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;

    // Size
    marker.scale.x = 0.1;

    // Attitude
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;

    // Color
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;
    marker.color.a = a;

    marker.lifetime = ros::Duration();

    return marker;
}

void
Pose::update_line(visualization_msgs::Marker &line) const
{
    // Add point
    geometry_msgs::Point p;
    p.x = _x;
    p.y = _y;
    p.z = _z;
    line.points.emplace_back(p);
}

void
Pose::to2d(Eigen::Affine2d &aff) const
{
    const Eigen::Affine3d af = affine();
    const double theta = atan2(af.rotation()(1, 0), af.rotation()(0, 0));
    const double c = cos(theta);
    const double s = sin(theta);
    aff.linear() << c, -s, s, c;
    aff.translation() = af.translation().block<2, 1>(0, 0);
    aff(2, 2) = 1;
}

void
Pose::from2d(const Eigen::Affine2d &aff)
{
    Eigen::Matrix3d rot = Eigen::Matrix3d::Identity();
    rot.block<2, 2>(0, 0) = aff.rotation();
    _x = aff(0, 2);
    _y = aff(1, 2);
    _z = 0;
    _q = Q(rot);
}

bool
Pose::operator==(const Pose &pose) const
{
    return p() == pose.p() && _q.w() == pose.q0() && _q.x() == pose.q1() && _q.y() == pose.q2() && _q.z() == pose.q3();
}

Pose::P
Pose::operator*(const P &p) const
{
    return T(p);
}
