#include <utils.h>

// Private functions
namespace {}


double
dist_minkowski(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1,
               const uint8_t order)
{
    return std::pow(std::pow(std::abs(x1 - x0), order) + std::pow(std::abs(y1 - y0), order), 1. / order);
}

double
dist_minkowski(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1,
               const double &z0,
               const double &z1,
               const uint8_t order)
{
    return std::pow(std::pow(std::abs(x1 - x0), order) + std::pow(std::abs(y1 - y0), order) + std::pow(std::abs(z1 - z0), order), 1. / order);
}

double
dist_manhattan(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1)
{
    return dist_minkowski(x0, x1, y0, y1, 1);
}

double
dist_manhattan(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1,
               const double &z0,
               const double &z1)
{
    return dist_minkowski(x0, x1, y0, y1, z0, z1, 1);
}

double
dist_euclidean(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1)
{
    return dist_minkowski(x0, x1, y0, y1, 2);
}

double
dist_euclidean(const double &x0,
               const double &x1,
               const double &y0,
               const double &y1,
               const double &z0,
               const double &z1)
{
    return dist_minkowski(x0, x1, y0, y1, z0, z1, 2);
}

float
get_angle(const Eigen::Vector2d &a,
          const Eigen::Vector2d &b)
{
    return atan2(a[0] * b[1] - a[1] * b[0], a.dot(b));
}
