#include <point.h>

// Private functions
namespace {}


/* Point2d */
Point2d::Point2d()
    : Eigen::Vector2d{} {}

Point2d::Point2d(const double &x,
                 const double &y)
    : Eigen::Vector2d{x, y} {}

Point2d::Point2d(const Pose &pose)
    : Point2d{pose.x(), pose.y()} {}

Point2d::Point2d(const Point3d &p)
    : Point2d{p.x(), p.y()} {}

Point2d::Point2d(const Eigen::Vector2d &p)
    : Point2d{p[0], p[1]} {}

Point2d::Point2d(const mt::Point &p)
    : Point2d{p.x, p.y} {}

double
Point2d::dist_to(const P &p) const {
    return dist_euclidean(x(), p.x(), y(), p.y());
}

visualization_msgs::Marker
Point2d::toMarker(const uint16_t &id,
                  const float &duration,
                  const float &r,
                  const float &g,
                  const float &b,
                  const float &a) const {
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = "linescanner_points";
    marker.id = id;
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;

    // Position
    marker.pose.position.x = x();
    marker.pose.position.y = y();
    marker.pose.position.z = 0;

    // Attitude
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    // Size
    const float scale = 0.01;
    marker.scale.x = scale;
    marker.scale.y = scale;
    marker.scale.z = scale;

    // Color
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;
    marker.color.a = a;

    marker.lifetime = ros::Duration(duration);

    return marker;
}

visualization_msgs::MarkerArray
Point2d::points2markers(const std::vector<P> &points,
                        const float &duration,
                        const float &r,
                        const float &g,
                        const float &b,
                        const float &a)
{
    visualization_msgs::MarkerArray ma;
    for (const P &p : points)
        ma.markers.emplace_back(p.toMarker(ma.markers.size(), duration, r, g, b, a));
    return ma;
}


/* Point3d */
Point3d::Point3d()
    : Eigen::Vector3d{} {}

Point3d::Point3d(const double &x,
                 const double &y,
                 const double &z)
    : Eigen::Vector3d{x, y, z} {}

Point3d::Point3d(const Pose &pose)
    : Point3d{pose.x(), pose.y(), pose.z()} {}

Point3d::Point3d(const Eigen::Vector3d &p)
    : Point3d{p[0], p[1], p[2]} {}

Point3d::Point3d(const Point2d &p)
    : Point3d{p.x(), p.y(), 0} {}

Point3d::Point3d(const geometry_msgs::Point &p)
    : Point3d(p.x, p.y, p.z) {}

double
Point3d::dist_to(const P &p) const
{
    return dist_euclidean(x(), p.x(), y(), p.y(), z(), p.z());
}

visualization_msgs::Marker
Point3d::toMarker(const uint16_t &id,
                  const float &duration,
                  const float &r,
                  const float &g,
                  const float &b,
                  const float &a) const
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = "linescanner_points";
    marker.id = id;
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;

    // Position
    marker.pose.position.x = x();
    marker.pose.position.y = y();
    marker.pose.position.z = z();

    // Attitude
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    // Size
    const float scale = 0.01;
    marker.scale.x = scale;
    marker.scale.y = scale;
    marker.scale.z = scale;

    // Color
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;
    marker.color.a = a;

    marker.lifetime = ros::Duration(duration);

    return marker;
}
