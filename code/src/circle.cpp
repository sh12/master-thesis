#include <circle.h>

// Private functions
namespace {}


// Constructors
Circle::Circle()
    : Circle{P(0, 0), 0} {}

Circle::Circle(const P &center, const double &radius)
    : _center{center}, _radius{radius} {}

Circle::Circle(const P &p1,
               const P &p2,
               const P &p3)
{
    // Following https://www.geeksforgeeks.org/equation-of-circle-when-three-points-on-the-circle-are-given/
    const P v12 = Eigen::Vector2d(p1 - p2);
    const P v13 = Eigen::Vector2d(p1 - p3);
    const P v31 = Eigen::Vector2d(p3 - p1);
    const P v21 = Eigen::Vector2d(p2 - p1);

    const double sx13 = pow(p1.x(), 2) - pow(p3.x(), 2);
    const double sy13 = pow(p1.y(), 2) - pow(p3.y(), 2);
    const double sx21 = pow(p2.x(), 2) - pow(p1.x(), 2);
    const double sy21 = pow(p2.y(), 2) - pow(p1.y(), 2);

    const double f = ((sx13 + sy13) * v12.x() + (sx21 + sy21) * v13.x())
            / (2 * (v31.y() * v12.x() - v21.y() * v13.x()));
    const double g = ((sx13 + sy13) * v12.y() + (sx21 + sy21) * v13.y())
            / (2 * (v31.x() * v12.y() - v21.x() * v13.y()));

    const double c = -pow(p1.x(), 2) - pow(p1.y(), 2) - 2 * g * p1.x() - 2 * f * p1.y();
    const double sqr_of_r = pow(-g, 2) + pow(-f, 2) - c;

    _center = P(-g, -f);
    _radius = sqrt(sqr_of_r);
}

Circle::Circle(const std::vector<P> &points)
{
    // Circle least squares estimation
    const P *p;
    Eigen::MatrixXd A(points.size(), 3);
    Eigen::VectorXd b(points.size());
    for (uint16_t i = 0; i < points.size(); ++i) {
        p = &(points[i]);
        A.block<1, 3>(i, 0) << 2 * p->x(), 2 * p->y(), 1;
        b(i) = pow(p->x(), 2) + pow(p->y(), 2);
    }
    const Eigen::Vector3d x = (A.transpose() * A).ldlt().solve(A.transpose() * b);

    _center = P(x[0], x[1]);
    _radius = sqrt(x[2] + pow(x[0], 2) + pow(x[1], 2));
}

Circle::Circle(const C &c)
    : Circle{c.center(), c.radius()} {}

Circle::Circle(const mt::Circle &c)
    : Circle{P(c.x, c.y), c.r} {}

double
Circle::dist_to(const P &p) const
{
    return p.dist_to(_center) - _radius;
}

double
Circle::dist_to(const Line2d &l) const
{
    return l.dist_to(_center) - _radius;
}


// Functions
visualization_msgs::Marker
Circle::toMarker(const uint16_t &id,
                 const float &duration,
                 const float &r,
                 const float &g,
                 const float &b,
                 const float &a) const
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = "linescanner_circles";
    marker.id = id;
    marker.type = visualization_msgs::Marker::CYLINDER;
    marker.action = visualization_msgs::Marker::ADD;

    // Position
    marker.pose.position.x = _center.x();
    marker.pose.position.y = _center.y();
    marker.pose.position.z = 5;

    // Attitude
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    // Size
    const double scale = _radius * 2;
    marker.scale.x = scale;
    marker.scale.y = scale;
    marker.scale.z = 10;

    // Color
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;
    marker.color.a = a;

    marker.lifetime = ros::Duration(duration);

    return marker;
}

std::pair<std::unique_ptr<Circle::P>, std::unique_ptr<Circle::P>>
Circle::intersection(const Line2d &l) const
{
    // Following https://mathworld.wolfram.com/Circle-LineIntersection.html
    // Shift points to around (0, 0)
    const P p(l.p() - _center);
    const P q(l.q() - _center);

    // Calculate determinant and helper variables
    const Eigen::Vector2d d = q - p;
    const double dr_sq = pow(d.x(), 2) + pow(d.y(), 2);
    const double D = p.x() * q.y() - q.x() * p.y();
    const double Delta = pow(_radius, 2) * dr_sq - pow(D, 2);

    // If no intersection
    if (Delta < 0)
        return std::make_pair(nullptr, nullptr);

    const int8_t sign = d.y() < 0 ? -1 : 1;
    const double Delta_sqrt = sqrt(Delta);
    std::unique_ptr<P> p0 = std::make_unique<P>((D * d.y() + sign * d.x() * Delta_sqrt) / dr_sq, (- D * d.x() + abs(d.y()) * Delta_sqrt) / dr_sq);
    *p0 += _center;

    // If only 1 intersection
    if (Delta == 0)
        return std::make_pair(std::move(p0), nullptr);

    // If 2 intersections
    std::unique_ptr<P> p1 = std::make_unique<P>((D * d.y() - sign * d.x() * Delta_sqrt) / dr_sq, (- D * d.x() - abs(d.y()) * Delta_sqrt) / dr_sq);
    *p1 += _center;

    return std::make_pair(std::move(p0), std::move(p1));
}

bool Circle::intersects(const Line2d &l) const
{
    // If circle distance to line positive, they don't intersect
    if (0 < dist_to(l))
        return false;

    // If negative, check if outside segment
    const float dp = dist_to(l.p());
    const float dq = dist_to(l.q());
    const float dd = l.length();

    if (dd < dp || dd < dq)
        return false;
    return true;
}
