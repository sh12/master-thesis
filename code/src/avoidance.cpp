#include <avoidance.h>

#include <deque>

// Constructors
Avoidance::Avoidance(ros::NodeHandle &nh)
    : _nh{nh},
      _obstaclesSub{_nh.subscribe<mt::Circles>("/" + OPTIONS::DRONE::NAME + "/sloam/circles", 1, &Avoidance::_obstaclesCb, this)},
//      _pointsSub{_nh.subscribe<mt::Points>("/" + OPTIONS::DRONE::NAME + "/local_points", 1, &Avoidance::_pointsCb, this)},
      _posSub{_nh.subscribe<geometry_msgs::PoseStamped>("/" + OPTIONS::DRONE::NAME + "/sloam/pose", 1, &Avoidance::_posCb, this)},
      _closestObstaclesPub{_nh.advertise<visualization_msgs::Marker>("/" + OPTIONS::DRONE::NAME + "/avoidance/closest_landmarks", 1)},
      _start{ros::Time::now()} {}

// Private functions
namespace {
struct Obstacle {
    C obstacle;
    float D;
};
typedef Obstacle O;

visualization_msgs::Marker
obstacles2markers(const Avoidance::P &cur_p, const std::pair<O, O> &obstacles)
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = "association";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::LINE_STRIP;
    marker.action = visualization_msgs::Marker::ADD;

    // Position
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;

    // Size
    marker.scale.x = 0.01;

    // Attitude
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;

    // Color
    marker.color.r = 1;
    marker.color.g = 1;
    marker.color.b = 0;
    marker.color.a = 1;

    marker.lifetime = ros::Duration(1 / OPTIONS::MISSION::UPDATE_RATE);

    // Add points
    auto add_point = [&marker](const Avoidance::P &p){
        geometry_msgs::Point gp;
        gp.x = p.x();
        gp.y = p.y();
        gp.z = OPTIONS::PLANNER::ALTITUDE;
        marker.points.emplace_back(gp);
    };
    add_point(obstacles.first.obstacle.center());
    add_point(cur_p);
    if (obstacles.second.D != FLT_MAX)
        add_point(obstacles.second.obstacle.center());

    return marker;
}

void
getClosestObstacles(const Avoidance::P &pos,
                    const std::vector<C> &obstacles,
                    std::pair<O, O> &obstacles_closest)
{
    // If only one obstacle, skip search
    if (obstacles.size() == 1) {
        obstacles_closest.first.obstacle = obstacles.front();
        obstacles_closest.first.D = obstacles_closest.first.obstacle.dist_to(pos);
        obstacles_closest.second.D = FLT_MAX;
        return;
    }

    // Store closest obstacle distances
    std::pair<float, float> d_min(FLT_MAX, FLT_MAX);
    std::pair<uint16_t, uint16_t> i_min;

    // Find closest obstacles
    for (uint16_t i = 0; i < obstacles.size(); ++i) {
        const C &o = obstacles[i];
        const float d = o.dist_to(pos);

        // If obstacle further away than considered distance, ignore it
        if (OPTIONS::AVOIDANCE::DIST_INFLUENCE < d)
            continue;

        // Else, compare it to other close obstacles
        if (d < d_min.first) {
            // Move closest to next-closest
            i_min.second = i_min.first;
            d_min.second = d_min.first;

            // Store new closest
            d_min.first = d;
            i_min.first = i;
        } else if (d < d_min.second) {
            // Store new next-closest
            d_min.second = d;
            i_min.second = i;
        }
    }

    // Store
    obstacles_closest.first.D = d_min.first;
    obstacles_closest.first.obstacle = obstacles[i_min.first];
    obstacles_closest.second.D = d_min.second;
    obstacles_closest.second.obstacle = obstacles[i_min.second];
}

bool
stuck(const Avoidance::P &p)
{
    // Memory
    static const uint16_t sz = OPTIONS::AVOIDANCE::STUCK_TIME * OPTIONS::MISSION::UPDATE_RATE;
    static std::deque<Avoidance::P> points;
    static Eigen::Vector2d mean(0, 0);

    // Accumulate points and calculate rolling mean
    if (points.size() == sz) {
        mean -= (points.front() - mean) / (points.size() - 1);
        points.pop_front();
    }
    mean += (p - mean) / (points.size() + 1);
    points.emplace_back(p);

    // If deque 'full'
    if (points.size() == sz) {
        // Estimate variance
        float var = 0;
        for (const Avoidance::P &point : points) {
            const Eigen::Vector2d deviation = point - mean;
            var += pow(deviation.x(), 2) + pow(deviation.y(), 2);
        }
        var /= points.size();

        if (var < 0.1)
            ROS_INFO("var = %f", var);

        // If variance less than threshold, consider drone stuck and reset variables
        if (var < OPTIONS::AVOIDANCE::STUCK_VAR_THRESH) {
            mean.x() = 0;
            mean.y() = 0;
            points.clear();
            return true;
        }
    }
    return false;
}
}

// Properties
void
Avoidance::set_target(const P &target)
{
    _origin = _target;
    _target = target;

    // Forget potential path generated by RRT
    _path.clear();

    // Set new orientation
    tf2::Quaternion quat;
    quat.setRPY(0, 0, atan2(_target.y() - _origin.y(),
                             _target.x() - _origin.x()));
    _quat = tf2::toMsg(quat);
}

// Functions
Avoidance::P
Avoidance::process()
{
    // Calculate sTw (world to SLOAM)
    Eigen::Affine2d wTs;
    Pose(_currentPos).to2d(wTs);
    const P p(wTs);
    const Eigen::Affine2d sTw = wTs.inverse();

    // Get points from linescanner
    std::vector<P> points(_currentPoints.points.size());
    for (uint16_t i = 0; i < _currentPoints.points.size(); ++i)
        points[i] = _currentPoints.points[i];

    // Get obstacles from SLOAM
    std::vector<C> obstacles(_currentObstacles.circles.size());
    for (uint16_t i = 0; i < _currentObstacles.circles.size(); ++i)
        obstacles[i] = _currentObstacles.circles[i];

    // Calculate target based on intersection between circle around drone and origin->target line
    P target = get_local_target(p);
    _errors.emplace_back(ros::Time::now() - _start, Line2d(_origin, _target).dist_to(p));

    // If stuck, calculate path with RRT
    if (!_path_found || stuck(p)) {
        ROS_WARN("STUCK!");
        _path_found = _rrt.search(p, target, obstacles, _path);
    }

    // If RRT path calculated, follow it
    while (!_path.empty() && _path.front().dist_to(p) < OPTIONS::AVOIDANCE::RRT::DIST_MAX)
        _path.erase(_path.begin());
    if (!_path.empty())
        target = _path.front();

    // Calculate target wrt. SLOAM
    const P uP2T = sTw * target;

    // If no obstacles, go directly to target
    if (_currentObstacles.circles.empty())
        return uP2T;

    // Else, find closest obstacles
    std::pair<O, O> obstacles_closest;
    getClosestObstacles(p, obstacles, obstacles_closest);
    const O &o0 = obstacles_closest.first;
    const O &o1 = obstacles_closest.second;

    // Return quick if no obstacles in range
    if (o0.D == FLT_MAX)
        return uP2T;

    // Visualise closest obstacles
    if (0 < _closestObstaclesPub.getNumSubscribers())
        _closestObstaclesPub.publish(obstacles2markers(p, obstacles_closest));

    // Calculate reference heading
    Eigen::Vector2d uUAVC;
    {
        auto calculate_uUAVT = [&](const C &o){
            // Also position-to-position unit vectors (1, 2)
            const P c = sTw * o.center();
            const Eigen::Vector2d uP2O = c.normalized();

            // Calculate distance to obstacle
            float D = c.norm() - o.radius();
            if (D < 0) D = 0;

            // Angle (4) - determines whether obstacle is in front of or behind + if left or right
            const float theta = get_angle(uP2T, uP2O);

            // uUAV (5)
            Eigen::Vector2d uUAV;
            if (theta < -M_PI_2 || M_PI_2 < theta)
                uUAV = uP2T.normalized();
            else {
                // Additionally, smoothen avoidance as to not go full circle
                float ratio = (D - OPTIONS::AVOIDANCE::DIST_PROXIMITY) / (OPTIONS::AVOIDANCE::DIST_INFLUENCE - OPTIONS::AVOIDANCE::DIST_PROXIMITY);
                if (ratio < 0) ratio = 0;
                else ratio = pow(ratio, 2);

                if (theta < 0)
                    uUAV = Eigen::Vector2d(-uP2O.y(), uP2O.x());
                else uUAV = Eigen::Vector2d(uP2O.y(), -uP2O.x());
                uUAV = uP2T.normalized() * ratio + uUAV * (1 - ratio);
            }

            // If closer than min proximity, move away from obstacle (7, 8)
            if (D < OPTIONS::AVOIDANCE::DIST_PROXIMITY) {
                float divisor = D - OPTIONS::DRONE::HITBOX;
                if (divisor < 1e-6) divisor = 1e-6;
                return Eigen::Vector2d(uUAV - ((_path.empty() ? OPTIONS::AVOIDANCE::DIST_PROXIMITY : OPTIONS::AVOIDANCE::RRT::DIST_PROXIMITY) - OPTIONS::DRONE::HITBOX) / divisor * uP2O);
            }
            else return uUAV;
        };

        // Calculate reference heading (6)
        const Eigen::Vector2d uUAVT0 = calculate_uUAVT(o0.obstacle);
        if (o1.D != FLT_MAX) {
            const Eigen::Vector2d uUAVT1 = calculate_uUAVT(o1.obstacle);
            uUAVC = (uUAVT0 * o1.D + uUAVT1 * o0.D).normalized();
        } else uUAVC = uUAVT0;
    }

    // Set out position
    return uUAVC;
}

Avoidance::P
Avoidance::get_local_target(const P &p) const
{
    if (_origin == _target)
        return _origin;

    // Calculate horison based on stuck counter
    const float horison = OPTIONS::AVOIDANCE::DIST_HORISON + (_rrt.stuck_counter() - 1) * OPTIONS::AVOIDANCE::RRT::DIST_HORISON_STUCK;
    if (p.dist_to(_target) < horison)
        return _target;

    const C c_lookahead(p, horison);
    const Line2d o2t(_origin, _target);
    std::pair<std::unique_ptr<P>, std::unique_ptr<P>> ps_intersect = c_lookahead.intersection(o2t);
    if (ps_intersect.second == nullptr) {
        if (ps_intersect.first == nullptr)
            return o2t.project(p);
        else return *ps_intersect.first;
    } else return ps_intersect.first->dist_to(_target) < ps_intersect.second->dist_to(_target) ? *ps_intersect.first : *ps_intersect.second;
}
