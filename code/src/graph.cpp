#include <graph.h>

// Local includes
#include <avoidance.h>

/* Node */
// Constructors
RRT::Node::Node(const P &p,
                Node *parent)
    : _parent{parent},
      _p{p} {}

// Private functions
namespace {}

// Properties
std::vector<RRT::Node*>
RRT::Node::leaves() const
{
    std::vector<Node*> leaves;
    get_leaves(leaves);
    return leaves;
}

void
RRT::Node::get_leaves(std::vector<Node*> &leaves) const
{
    for (const std::unique_ptr<Node> &child : _children) {
        if (child->is_leaf())
            leaves.emplace_back(child.get());
        else child->get_leaves(leaves);
    }
}

float
RRT::Node::cost() const
{
    float cost = 0;
    if (_parent != nullptr)
        get_cost(cost);
    return cost;
}

void
RRT::Node::get_cost(float &cost) const
{
    cost += _p.dist_to(_parent->p());
    if (_parent->parent() != nullptr)
        _parent->get_cost(cost);
}

uint16_t RRT::Node::level() const
{
    uint16_t level = 0;
    get_level(level);
    return level;
}

void RRT::Node::get_level(uint16_t &level) const
{
    if (_parent != nullptr)
        _parent->get_level(++level);
}

size_t
RRT::Node::size() const
{
    size_t size = 1;
    get_size(size);
    return size;
}

void
RRT::Node::get_size(size_t &size) const
{
    size += _children.size();
    for (const std::unique_ptr<Node> &child : _children)
        child->get_size(size);
}

// Functions
RRT::Node*
RRT::Node::add_child(const RRT::P &child)
{
    _children.emplace_back(std::make_unique<Node>(child, this));
    return _children.back().get();
}

RRT::Node*
RRT::Node::find(const P &p) const
{
    for (const std::unique_ptr<Node> &child : _children) {
        if (child->p() == p)
            return child.get();
        if (!child->is_leaf())
            return child->find(p);
    }
    return nullptr;
}

void
RRT::Node::print() const
{
    do_print(0, level());
    std::cout << std::flush;
}

void
RRT::Node::do_print(const uint8_t &rel_level,
                    const uint8_t &diff) const
{
    std::cout << std::string(rel_level, ' ') << "(" << _p.x() << " " << _p.y() << ") (" << cost() << ") #" << std::to_string(rel_level + diff);
    if (is_leaf())
        std::cout << " *";
    std::cout << " \n";
    for (const std::unique_ptr<Node> &child : _children)
        child->do_print(rel_level + 1, diff);
}

void
RRT::Node::path_from_root(std::vector<RRT::P> &path) const
{
    uint16_t lvl = level();
    path.resize(lvl + 1);
    get_path_to_root(path, lvl);
}

void
RRT::Node::get_path_to_root(std::vector<RRT::P> &path,
                            uint16_t &level) const
{
    path[level] = _p;
    if (_parent != nullptr)
        _parent->get_path_to_root(path, --level);
}

void
RRT::Node::give_child(Node *child,
                      Node *parent)
{
    child->_parent = parent;
    std::vector<std::unique_ptr<Node>>::iterator it = std::find_if(_children.begin(), _children.end(), [&child](std::unique_ptr<Node> &c){return c.get() == child;});
    parent->_children.emplace_back(std::move(*it));
    _children.erase(it);
}


/* Graph */
// Constructors
RRT::RRT() {
    reset();
}

// Private functions

namespace {
bool
obstructed(const Line2d &l,
           const std::vector<C> &obstacles)
{
    for (const C &o : obstacles) {
        if (o.intersects(l))
            return true;
    }
    return false;
}
}

// Properties

// Functions
bool
RRT::search(const P &start,
            const P &target,
            std::vector<C> obstacles,
            std::vector<P> &path)
{
    set_problem(start, target);

    // Prepare obstacles for search
    const float obstacle_pad = OPTIONS::DRONE::HITBOX + (OPTIONS::AVOIDANCE::RRT::DIST_PROXIMITY - OPTIONS::DRONE::HITBOX) / stuck_counter();
    for (C &o : obstacles)
        o.radius() += obstacle_pad;

    // Random point generator
    Eigen::Affine2d wTl;
    const Eigen::Vector2d diff = _target - _root->p();
    const float pad = stuck_counter() * OPTIONS::AVOIDANCE::RRT::AREA_PADDING;
    const float length = diff.norm() + 2 * pad;
    const float width = 2 * pad;
    {
        // Build transformation matrix
        const float theta = atan2(diff.y(), diff.x());
        const float c = cos(theta);
        const float s = sin(theta);
        wTl.linear() << c, -s, s, c;
        wTl.translation() << _root->p();
    }
    auto generate_random_point = [&wTl, &length, &width, &pad](){
        const P p(rand() / (float) RAND_MAX * length - pad, rand() / (float) RAND_MAX * width - pad);
        return wTl * p;
    };

    // Allowance increase
    float target_allowance = OPTIONS::AVOIDANCE::RRT::DIST_MAX;
    for (const C &o : obstacles) {
        const float d = o.dist_to(_target);
        if (d < 0)
            target_allowance -= d;
    }

    // Store node target
    Node *node_target = nullptr;

    // Run RRT
    std::vector<int> k_indices;
    std::vector<float> k_dists;
    bool target_found = false;
    uint16_t iterations = 0, extra_iterations = 0;
    const ros::Time t_start = ros::Time::now();
    auto keep_running = [&t_start]()->bool{return ros::ok() && ros::Time::now() - t_start < ros::Duration(OPTIONS::AVOIDANCE::RRT::TIME_RUN_MAX);};
    pclP p_pcl;
    while (keep_running()) {
        // Find closest visible node
        P p_rand;
        {
            // Generate random point
            p_rand = generate_random_point();

            // Find nearest node_
            p_pcl.x = p_rand.x();
            p_pcl.y = p_rand.y();
            _kdtree.nearestKSearch(p_pcl, 1, k_indices, k_dists);

            // References to closest node
            Node *node_closest = _node_lookup[k_indices.front()];
            const P &p_closest = node_closest->p();

            // Limit to max dist from closest point
            Eigen::Vector2d d = p_rand - p_closest;
            const float dist = d.norm();
            if (OPTIONS::AVOIDANCE::RRT::DIST_MAX < dist) {
                d *= OPTIONS::AVOIDANCE::RRT::DIST_MAX / dist;
                p_rand = P(p_closest + d);
            }
        }

        // RRT*: Keep generating random points until rrtstar can connect it
        if (!rrtstar(p_rand, obstacles))
            continue;

        // If target reached, add it to tree
        if (target_found)
            ++extra_iterations;
        else {
             ++iterations;
            if (!obstructed(Line2d(p_rand, _target), obstacles) || p_rand.dist_to(_target) < target_allowance) {
                node_target = _node_lookup.back()->add_child(_target);
                cloud_add_node(node_target);
                target_found = true;
                ROS_INFO("Target found!");
            }
        }
    }
    const uint16_t sum = iterations + extra_iterations;
    ROS_INFO("RRT* iterations: %d + %d = %d (+%.2f%%)", iterations, extra_iterations, sum, (float) extra_iterations  / iterations * 100);


    if (true) {
        _root->print();

        std::vector<Node*> leaves = _root->leaves();
        std::cout << "tree = [";
        for (const Node * const leaf : leaves) {
            std::vector<P> path_l;
            leaf->path_from_root(path_l);
            std::cout << "[";
            for (const P &p : path_l)
                std::cout << "(" << p.x() << "," << p.y() << "),";
            std::cout << "],";
        }
        std::cout << "]\n";

        std::cout << "obstacles = [";
        for (const C &o : obstacles)
            std::cout << "((" << o.center().x() << "," << o.center().y() << ")," << o.radius() << "),";
        std::cout << "]\n";

        std::cout << "obstacle_pad = " << obstacle_pad << "\n";

        std::cout << "start = (" << _root->p().x() << "," << _root->p().y() << ")\n";
        std::cout << "end = (" << _target.x() << "," << _target.y() << ")\n";
        P a(wTl * P(-pad, pad));
        P b(wTl * P(length - pad, pad));
        P c(wTl * P(length - pad, -pad));
        P d(wTl * P(-pad, -pad));
        std::cout << "arena = [(" << a.x() << "," << a.y() << "),(" << b.x() << "," << b.y() << "),(" << c.x() << "," << c.y() << "),(" << d.x() << "," << d.y() << ")]\n";

        {
            std::vector<P> path_l;
            std::cout << "path = [";
            if (node_target != nullptr)
                node_target->path_from_root(path_l);
            for (const P &p : path_l)
                std::cout << "(" << p.x() << "," << p.y() << "),";
            std::cout << "]\n";
        }

        std::cout << std::endl;
    }

    // If target found
    if (node_target != nullptr) {
        node_target->path_from_root(path);
        reset();
        return true;
    }
    else {
        ROS_WARN("No viable RRT* path found. Increasing multiplier to %d!", ++_stuck_multiplier);
        if (3 < stuck_counter())
            throw std::invalid_argument("TOO HIGH MULTIPLIER");
        return false;
    }
}

void
RRT::set_problem(const P &start,
                 const P &target)
{
    ROS_INFO("RRT: (%f %f) -> (%f %f)", start.x(), start.y(), target.x(), target.y());

    if (_root == nullptr) {
        _root = std::make_unique<Node>(start);
        cloud_add_node(_root.get());
    } else {
        _root->p() = start;
        pclP &p = _cloud->front();
        p.x = start.x();
        p.y = start.y();
        _kdtree.setInputCloud(_cloud);
    }
    _target = target;
}

bool
RRT::rrtstar(const P &p,
             std::vector<C> &obstacles)
{
    // https://arxiv.org/pdf/1005.0416.pdf algorithm 4
    pclP p_pcl;
    p_pcl.x = p.x();
    p_pcl.y = p.y();
    std::vector<int> k_indices;
    std::vector<float> k_dists;
    _kdtree.radiusSearch(p_pcl, OPTIONS::AVOIDANCE::RRT::DIST_RRTSTAR, k_indices, k_dists);
    for (float &d : k_dists)
        d = sqrt(d);

    // Find path with lowest cost to new point
    int i_min = -1;
    for (uint16_t i = 0; i < k_indices.size(); ++i) {
        // Get reference
        Node *n_near = _node_lookup[k_indices[i]];

        // Compare costs
        if (!obstructed(Line2d(p, n_near->p()), obstacles) && (i_min == -1 || n_near->cost() + k_dists[i] < _node_lookup[k_indices[i_min]]->cost() + k_dists[i_min]))
            i_min = i;
    }
    if (i_min == -1)
        return false;

    // Add child to best parent
    Node *n_new = _node_lookup[k_indices[i_min]]->add_child(p);
    cloud_add_node(n_new);

    // Optimize other nearest points
    for (uint16_t i = 0; i < k_indices.size(); ++i) {
        // Skip parent
        if (i == i_min)
            continue;

        // Get reference
        Node *n_near = _node_lookup[k_indices[i]];

        // Compare costs
        if (!obstructed(Line2d(n_new->p(), n_near->p()), obstacles) && n_new->cost() + k_dists[i] < n_near->cost())
            n_near->parent()->give_child(n_near, n_new);
    }
    return true;
}

void
RRT::cloud_add_node(Node * const n)
{
    // Add point to kdtree
    pclP p_pcl;
    p_pcl.x = n->p().x();
    p_pcl.y = n->p().y();
    _cloud->emplace_back(p_pcl);
    _kdtree.setInputCloud(_cloud);

    // Store references from cloud to node
    _node_lookup.emplace_back(n);
}

void
RRT::reset() {
    _root = nullptr;
    _cloud->clear();
    _node_lookup.clear();
    _stuck_multiplier = 1;
}
