#include <planner.h>
#include <sloam.h>

// Private functions
namespace {
visualization_msgs::Marker
path2marker(const std::vector<Point3d> &points,
            const std::string &ns,
            const float &r,
            const float &g,
            const float &b,
            const float &a,
            const uint16_t &id = 0)
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = ns;
    marker.id = id;
    marker.type = visualization_msgs::Marker::LINE_STRIP;
    marker.action = visualization_msgs::Marker::ADD;

    // Position
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;

    // Size
    marker.scale.x = 0.05;

    // Attitude
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;

    // Color
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;
    marker.color.a = a;

    marker.lifetime = ros::Duration();

    // Add points
    geometry_msgs::Point geom_p;
    for (const Point3d &p : points) {
        geom_p.x = p.x();
        geom_p.y = p.y();
        geom_p.z = p.z();
        marker.points.emplace_back(geom_p);
    }
    marker.points.emplace_back(marker.points.front());

    return marker;
}

visualization_msgs::Marker
path2marker(const std::vector<Planner::P> &points,
            const std::string &ns,
            const float &r,
            const float &g,
            const float &b,
            const float &a,
            const uint16_t &id = 0)
{
    std::vector<Point3d> points_3d;
    for (const Planner::P &p : points)
        points_3d.emplace_back(p);
    return path2marker(points_3d, ns, r, g, b, a, id);
}

void
paths2markers(const std::vector<std::vector<Planner::P>> &obstacles,
                  visualization_msgs::MarkerArray &ma,
                  const std::string &ns,
                  const float &r,
                  const float &g,
                  const float &b,
                  const float &a)
{
    ma.markers.clear();
    for (const std::vector<Planner::P> &obstacle : obstacles)
        ma.markers.emplace_back(path2marker(obstacle, ns, r, g, b, a, ma.markers.size()));
}

void
multi2single(std::vector<Planner::P> &boundary,
             const std::vector<std::vector<Planner::P>> &obstacles)
{
    // MISSING: IF MANY OBSTACLES, THEY CAN'T CONTINUE THE CONNECTION
    // MISSING: IF CLOSEST VERTEX IS NOT VISIBLE, IT RUINS THE BOUNDARY

    typedef pcl::PointXY P;
    typedef pcl::PointCloud<P> C;

    // Generate kdtree
    pcl::search::KdTree<P> kdtree(false);
    P *p;
    C::Ptr cloud(new C(boundary.size(), 1));
    {
        uint16_t i = 0;
        while (i < boundary.size()) {
            p = &(*cloud)[i];
            p->x = boundary[i].x();
            p->y = boundary[i].y();
            ++i;
        }
        kdtree.setInputCloud(cloud);
    }

    {
        // For every obstacle, find closest vertex to boundary vertex
        const uint8_t K = 1;
        std::vector<int> nn_idcs(K);
        std::vector<float> nn_dists(K);
        float min_d;
        uint16_t min_b_idx;
        uint8_t min_o_idx;
        uint16_t shift = 0;

        // For every obstacle
        for (const std::vector<Planner::P> &obstacle : obstacles) {
            min_d = FLT_MAX;

            // For every vertex in obstacle, find nearest boundary vertex
            for (uint8_t i = 0; i < obstacle.size(); ++i) {
                const Planner::P &p_o = obstacle[i];
                p->x = p_o.x();
                p->y = p_o.y();
                kdtree.nearestKSearch(*p, K, nn_idcs, nn_dists);

                if (nn_dists.front() < min_d) {
                    min_d = nn_dists.front();
                    min_b_idx = nn_idcs.front();
                    min_o_idx = i;
                }
            }

            // Shift boundary indices to handle previous obstacle insertions
            min_b_idx += shift;
            shift += obstacle.size() + 2;

            // Order obstacle vertices for insertion
            std::vector<Planner::P> obstacle_ordered;
            {
                uint8_t i = min_o_idx;
                obstacle_ordered.emplace_back(boundary[min_b_idx]);
                while (obstacle_ordered.size() < obstacle.size() + 2) {
                    obstacle_ordered.emplace_back(obstacle[i]);
                    i = i == 0 ? obstacle.size() - 1 : i - 1;
                }
            }

            // Insert obstacle
            boundary.insert(boundary.begin() + min_b_idx, obstacle_ordered.begin(), obstacle_ordered.end());
        }
    }
}

enum ORIENTATION {
    COLLINEAR,
    CW,
    CCW
};
ORIENTATION
orientation(const Planner::P &p0,
            const Planner::P &p1,
            const Planner::P &p2)
{
    const float val = (p1.y() - p0.y()) * (p2.x() - p1.x()) - (p1.x() - p0.x()) * (p2.y() - p1.y());
    if (val == 0)
        return COLLINEAR;
    return 0 < val ? CW : CCW;
}

bool
inside_triangle(const Planner::P t0,
                const Planner::P t1,
                const Planner::P t2,
                const Planner::P p)
{
    // https://stackoverflow.com/a/2049593/2546414
    const ORIENTATION o = orientation(p, t0, t1);
    if (o == orientation(p, t1, t2) && o == orientation(p, t2, t0))
        return true;
    return false;
}

bool
intersects(const Planner::P &p0s,
           const Planner::P &p0e,
           const Planner::P &p1s,
           const Planner::P &p1e)
{
    // https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
    if (orientation(p0s, p1s, p0e) != orientation(p0s, p1e, p0e)
        && orientation(p1s, p0s, p1e) != orientation(p1s, p0e, p1e))
        return true;
    return false;
}

void
do_decompose_concave(const std::vector<Planner::P> &boundary,
                     std::vector<std::vector<Planner::P>> &boundaries_convex)
{
    if (!ros::ok())
        return;

    // Helper variables
    const Planner::P *p, *c, *n;
    Eigen::Vector2d pc, nc;
    auto set_references = [&boundary, &p, &c, &n, &pc, &nc](const uint16_t &i)
    {
        p = &boundary[i == 0 ? boundary.size() - 1 : i - 1];
        c = &boundary[i];
        n = &boundary[i == boundary.size() - 1 ? 0 : i + 1];

        // Calculate vectors
        pc = *c - *p;
        nc = *c - *n;
    };

    // Find concave indices
    std::set<uint16_t> conc_idcs;
    for (uint16_t i = 0; i < boundary.size(); ++i) {
        // Set reference points and vectors
        set_references(i);

        const float theta = get_angle(pc, nc);
        if (theta < 0)
            conc_idcs.insert(i);
    }

    // If no concave indices, the boundary is convex
    if (conc_idcs.empty()) {
        boundaries_convex.emplace_back(boundary);
        return;
    }

    // Find visible indices
    const uint16_t i = *conc_idcs.begin();
    std::set<uint16_t> vis_idcs;
    {
        bool visible;
        std::set<uint16_t> vis_idcs_pre;
        set_references(i);

        // For all other indices
        for (uint16_t j = 0; j < boundary.size(); ++j) {
            const Planner::P &pj = boundary[j];

            // Skip neighbours
            if ((i - 1 <= j && j <= i + 1) || *c == pj)
                continue;

            // Check if pj in front of pi
            {
                visible = !intersects(*c, pj, *p, *n);
                if (!visible)
                    continue;
            }

            // Check if pi in front of pj
            {
                const Planner::P &pjp = boundary[j == 0 ? boundary.size() - 1 : j - 1];
                const Planner::P &pjn = boundary[j == boundary.size() - 1 ? 0 : j + 1];
                visible = intersects(*c, pj, pjp, pjn);

                // If pj concave vertex, the lines should not intersect
                if (conc_idcs.find(j) != conc_idcs.end())
                    visible = !visible;
                else if (!visible) {
                    // Else if pj convex vertex AND if not intersecting, pi might lie inside pjp-pj-pjn triangle
                    visible = inside_triangle(pjp, pj, pjn, *c);
                }

                if (!visible)
                    continue;
            }

            // If not behind, check if intersects with other lines
            for (uint16_t k = 0; k < boundary.size(); ++k) {
                // Store references
                const Planner::P &pk0 = boundary[k];
                const Planner::P &pk1 = boundary[(k + 1) % boundary.size()];

                // Skip neighbours to pi and pj
                if (pk0 == *c || pk1 == *c || pk0 == pj || pk1 == pj)
                    continue;

                // Skip if any of the points are copies of current concave point
                if (*c == pk0 || *c == pk1)
                    continue;

                // If any intersections, the point is not visible
                if (intersects(*c, pj, pk0, pk1)) {
                    visible = false;
                    break;
                }
            }

            // If still visible, add it
            if (visible)
                vis_idcs_pre.emplace(j);
        }

        // Keep only concave indices, if any
        for (const uint16_t &j : conc_idcs) {
            if (j == i)
                continue;

            if (vis_idcs_pre.find(j) != vis_idcs_pre.end())
                vis_idcs.emplace(j);
        }

        // If no concave indices, keep all
        if (vis_idcs.empty())
            vis_idcs = vis_idcs_pre;
    }

    // Get best visible vertex
    uint16_t i_min;
    {
        // Get bisector
        const Eigen::Vector2d bisector = pc.norm() * nc + nc.norm() * pc;
        float alpha_min = FLT_MAX;
        for (const uint16_t &vis_i : vis_idcs) {
            const Eigen::Vector2d cv = boundary[vis_i] - *c;
            const float alpha = abs(get_angle(bisector, cv));
            if (alpha < alpha_min) {
                alpha_min = alpha;
                i_min = vis_i;
            }
        }
    }

    // Do decomposition
    {
        std::vector<Planner::P> boundary_first, boundary_second;

        // Build subpolygons
        for (uint16_t j = 0; j < boundary.size(); ++j) {
            const Planner::P &p = boundary[j];
            if (j == i || j == i_min) {
                boundary_first.emplace_back(p);
                boundary_second.emplace_back(p);
                continue;
            }

            ((i < j && j < i_min) || (i_min < j && j < i) ? boundary_second : boundary_first).emplace_back(p);
        }

        // Recurse
        do_decompose_concave(boundary_first, boundaries_convex);
        do_decompose_concave(boundary_second, boundaries_convex);
    }
}

void
decompose_concave(const std::vector<Planner::P> &boundary,
                  std::vector<std::vector<Planner::P>> &boundaries_convex)
{
    boundaries_convex.clear();

    do_decompose_concave(boundary, boundaries_convex);
}

void
solve_tsp(const std::vector<std::vector<Planner::P>> &boundaries,
          std::vector<uint16_t> &path,
          const uint16_t &start)
{
    // Lambda for getting smallest distance between boundaries
    auto get_dist = [](const std::vector<Planner::P> &bi, const std::vector<Planner::P> &bj)->float{
        float min_d = FLT_MAX;
        bool found0 = false;
        for (const Planner::P &pi : bi) {
            for (const Planner::P &pj : bj) {
                const float d = pi.dist_to(pj);

                // If 2 zeros found, border is a line and should prefer these
                if (d == 0) {
                    if (found0)
                        return -1;
                    found0 = true;
                }

                if (d < min_d)
                    min_d = d;
            }
        }
        return min_d;
    };

    // Calculate distance matrix
    Eigen::MatrixXf dmat(boundaries.size(), boundaries.size());
    for (uint16_t i = 0; i < boundaries.size(); ++i) {
        const std::vector<Planner::P> &bi = boundaries[i];

        // 0s in diagonal
        dmat(i, i) = 0;

        for (uint16_t j = i + 1; j < boundaries.size(); ++j) {
            const std::vector<Planner::P> &bj = boundaries[j];

            // Store distance
            dmat(i, j) = dmat(j, i) = get_dist(bi, bj);
        }
    }

    // Create vector of indices for permutations
    std::vector<uint16_t> v;
    for (uint16_t i = 0; i < boundaries.size(); ++i) {
        if (i != start)
            v.emplace_back(i);
    }

    // BF solve TSP
    {
        path.front() = start;
        float min_cost = FLT_MAX;
        do {
            uint16_t i = start;
            float cost = 0;
            for (const uint16_t &j : v) {
                cost += dmat(i, j);
                i = j;
            }
            cost += dmat(i, start);

            if (cost < min_cost) {
                min_cost = cost;
                std::copy(v.begin(), v.end(), path.begin() + 1);
            }
        } while (ros::ok() && std::next_permutation(v.begin(), v.end()));
    }
}

Planner::P
intersect(const Planner::P &p0,
          const Planner::P &q0,
          const Planner::P &p1,
          const Planner::P &q1)
{
    // https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#Given_two_points_on_each_line_segment
    const float t = ((p0.x() - p1.x()) * (p1.y() - q1.y()) - (p0.y() - p1.y()) * (p1.x() - q1.x())) / ((p0.x() - q0.x()) * (p1.y() - q1.y()) - (p0.y() - q0.y()) * (p1.x() - q1.x()));
    return Planner::P(p0.x() + t * (q0.x() - p0.x()), p0.y() + t * (q0.y() - p0.y()));
}

void
plan_path(const std::vector<std::vector<Planner::P>> &boundaries,
          Mission &mission,
          const Planner::P &start)
{
    // Find closest polygon vertex
    uint16_t start_poly;
    {
        float min_d = FLT_MAX;
        for (uint16_t i = 0; i < boundaries.size(); ++i) {
            const std::vector<Planner::P> &boundary = boundaries[i];
            for (const Planner::P &p : boundary) {
                const float d = start.dist_to(p);
                if (d < min_d) {
                    min_d = d;
                    start_poly = i;
                }
            }
        }
    }

    // Get polygon visit order (Travelling Salesman Problem)
    std::vector<uint16_t> tsp_path(boundaries.size());
    solve_tsp(boundaries, tsp_path, start_poly);

    // Get information about all polygons
    struct poly_info {
        const uint16_t i, p_idx, q_idx, r_idx;
        float dist_pq2r;
        const std::map<uint16_t, uint16_t> borders;
    };
    std::vector<poly_info> polygons;
    {
        // Iterate all convex polygons
        for (uint16_t i = 0; i < tsp_path.size(); ++i) {
            const std::vector<Planner::P> &c = boundaries[tsp_path[i]];
            const std::vector<Planner::P> &n = boundaries[tsp_path[(i + 1) % tsp_path.size()]];

            // Get longest edge points + point furthest away from edge
            uint16_t p_idx, q_idx;
            {
                float d_max = FLT_MIN;
                for (uint16_t j = 0; j < c.size(); ++j) {
                    const uint16_t q_i = (j + 1) % c.size();
                    const float d = c[j].dist_to(c[q_i]);
                    if (d_max < d) {
                        d_max = d;
                        p_idx = j;
                        q_idx = q_i;
                    }
                }
            }
            const Planner::P &p = c[p_idx];
            const Planner::P &q = c[q_idx];

            // Get point furthest away from longest edge
            uint16_t r_idx;
            float dist_pq2r;
            {
                float d_max = FLT_MIN;
                for (uint16_t j = 0; j < c.size(); ++j) {
                    if (j == p_idx || j == q_idx)
                        continue;

                    const float d = Line2d(p, q).dist_to(c[j]);
                    if (d_max < d) {
                        d_max = d;
                        r_idx = j;
                    }
                }
                dist_pq2r = d_max;
            }

            // Get border point(s) to next boundary
            std::map<uint16_t, uint16_t> borders;
            {
                float d_min = FLT_MAX;
                for (uint16_t j = 0; j < c.size(); ++j) {
                    for (uint16_t k = 0; k < n.size(); ++k) {
                        const float d = c[j].dist_to(n[k]);

                        if (d < d_min) {
                            d_min = d;
                            borders = {std::make_pair(j, k)};
                        } else if (d == d_min)
                            borders[j] = k;
                    }
                }
            }

            // Store information
            polygons.emplace_back(poly_info{tsp_path[i], p_idx, q_idx, r_idx, dist_pq2r, borders});
        }
    }

    // Do planning
    {
        // Now we have boundary point(s), direction to scan in, point furthest away from longest edge
        // Start from p, q, or r and do scanning
        auto add_step = [](MissionSequence* const ms, const Planner::P &p)->void{
            ms->add_step(std::make_unique<MissionPoint>(p, OPTIONS::PLANNER::ALTITUDE));
        };

        add_step(&mission, start);
        for (uint16_t i = 0; i < polygons.size(); ++i) {
            const poly_info &poly = polygons[i];

            // References
            const std::vector<Planner::P> &c = boundaries[poly.i];
            const float &dist_pq2r = poly.dist_pq2r;
            const uint16_t &p_idx = poly.p_idx;
            const uint16_t &q_idx = poly.q_idx;
            const uint16_t &r_idx = poly.r_idx;
            const Planner::P &p = c[p_idx];
            const Planner::P &q = c[q_idx];
            const Planner::P &r = c[r_idx];
            const Planner::P &entry_p = mission.last_point()->p();
            const std::map<uint16_t, uint16_t> borders = poly.borders;

            // Choose start + end point as p, q or r by smallest distances
            uint16_t start_idx, end_idx;
            {
                // Calculate distances to entry
                const float dist_pe = p.dist_to(entry_p);
                const float dist_qe = q.dist_to(entry_p);
                const float dist_re = r.dist_to(entry_p);

                // Calculate distance to borders
                const uint16_t &b1 = borders.begin()->first;
                const float dist_pb1 = p.dist_to(c[b1]);
                const float dist_qb1 = q.dist_to(c[b1]);
                const float dist_rb1 = r.dist_to(c[b1]);

                // Combine distances
                float d_min = dist_pe + dist_rb1;
                uint8_t comb = 0;
                auto comb_test = [&d_min, &comb](const float &d, const uint8_t &j){
                    if (d < d_min) {
                        d_min = d;
                        comb = j;
                    }
                };
                comb_test(dist_qe + dist_rb1, 1);
                comb_test(dist_re + dist_pb1, 2);
                comb_test(dist_re + dist_qb1, 2);

                // Repeat if 2 borders
                const uint16_t &b2 = (--borders.end())->first;
                if (borders.size() == 2) {
                    const float dist_pb2 = p.dist_to(c[b2]);
                    const float dist_qb2 = q.dist_to(c[b2]);
                    const float dist_rb2 = r.dist_to(c[b2]);

                    // Combine distances
                    comb_test(dist_pe + dist_rb2, 3);
                    comb_test(dist_qe + dist_rb2, 4);
                    comb_test(dist_re + dist_pb2, 5);
                    comb_test(dist_re + dist_qb2, 5);
                }

                switch (comb) {
                case 0:
                    {
                        start_idx = p_idx;
                        end_idx = b1;
                    } break;

                case 1:
                    {
                        start_idx = q_idx;
                        end_idx = b1;
                    } break;

                case 2:
                    {
                        start_idx = r_idx;
                        end_idx = b1;
                    } break;

                case 3:
                    {
                        start_idx = p_idx;
                        end_idx = b2;
                    } break;

                case 4:
                    {
                        start_idx = q_idx;
                        end_idx = b2;
                    } break;

                case 5:
                    {
                        start_idx = r_idx;
                        end_idx = b2;
                    } break;

                default:
                    throw std::invalid_argument("Invalid distance combination: " + std::to_string(comb));
                    break;
                }
            }
            const Planner::P &start_p = c[start_idx];
            const Planner::P &end_p = c[end_idx];

            // Do the path planning
            {
                const uint16_t num_traverses = ceil(dist_pq2r / OPTIONS::PLANNER::PATH_SPACING);
                const double path_spacing = dist_pq2r / num_traverses;

                // Keep track of points
                uint8_t idx_cw = start_idx == c.size() - 1 ? 0 : start_idx + 1;
                uint8_t idx_ccw = start_idx == 0 ? c.size() - 1 : start_idx - 1;
                Planner::P p_cur_start, p_cur_end;

                // Figure out initial direction
                bool reverse;
                {
                    if (start_idx == p_idx)
                        reverse = true;
                    else if (start_idx == q_idx)
                        reverse = false;
                    else {
                        const bool even = num_traverses % 2 == 0;
                        if (end_idx == p_idx)
                            reverse = even;
                        else reverse = !even;
                    }
                }

                // Setup initial points
                if (start_idx == r_idx)
                    p_cur_start = p_cur_end = start_p;
                else {
                    p_cur_start = start_p;
                    p_cur_end = start_idx == p_idx ? q : p;
                }

                // Begin path planning of current convex polygon
                std::unique_ptr<MissionSequence> ms = std::make_unique<MissionSequence>();
                add_step(ms.get(), start_p);

                uint16_t count_traverses = 0;
                uint16_t cw_lim = (start_idx == r_idx ? p_idx : r_idx);
                uint16_t ccw_lim = (start_idx == r_idx ? q_idx : r_idx);
                const Planner::P &scan_end = c[start_idx == r_idx ? p_idx : r_idx];
                while (ros::ok()) {
                    // Figure out which point to move towards
                    {
                        const float dist_next2nextpoly = Line2d(p_cur_start, Planner::P(p_cur_start + q - p)).dist_to(scan_end) - path_spacing;
                        while (idx_cw != cw_lim && dist_next2nextpoly < Line2d(c[idx_cw], Planner::P(c[idx_cw] + q - p)).dist_to(scan_end)) {
                            if (reverse)
                                add_step(ms.get(), c[idx_cw]);
                            idx_cw = (idx_cw + 1) % c.size();
                        }
                        while (idx_ccw != ccw_lim && dist_next2nextpoly < Line2d(c[idx_ccw], Planner::P(c[idx_ccw] + q - p)).dist_to(scan_end)) {
                            if (!reverse)
                                add_step(ms.get(), c[idx_ccw]);
                            idx_ccw = idx_ccw == 0 ? c.size() - 1 : idx_ccw - 1;
                        }
                    }

                    // Store point references
                    const Planner::P &p_cw = c[idx_cw];
                    const Planner::P &p_cw_prev = c[idx_cw == 0 ? c.size() - 1 : idx_cw - 1];
                    const Planner::P &p_ccw = c[idx_ccw];
                    const Planner::P &p_ccw_prev = c[idx_ccw == c.size() - 1 ? 0 : idx_ccw + 1];

                    // Calculate vectors to next point
                    Eigen::Vector2d v_cw = p_cw - p_cw_prev;
                    Eigen::Vector2d v_ccw = p_ccw - p_ccw_prev;
                    v_cw = v_cw.normalized() * path_spacing / abs(sin(get_angle(p_cw_prev - p_cw, q - p)));
                    v_ccw = v_ccw.normalized() * path_spacing / abs(sin(get_angle(p - q, p_ccw_prev - p_ccw)));

                    // Calculate next point based on intersections of line to follow and previous traversal line
                    const Eigen::Vector2d p_cur_dir = p_cur_start + (q - p);
                    const Planner::P p_intersect_cw = intersect(p_cur_start, p_cur_dir, p_cw_prev, p_cw);
                    const Planner::P p_intersect_ccw = intersect(p_cur_start, p_cur_dir, p_ccw_prev, p_ccw);
                    if (reverse) {
                        p_cur_start = Planner::P(p_intersect_cw + v_cw);
                        p_cur_end = Planner::P(p_intersect_ccw + v_ccw);
                    } else {
                        p_cur_start = Planner::P(p_intersect_ccw + v_ccw);
                        p_cur_end = Planner::P(p_intersect_cw + v_cw);
                    }

                    // If finished
                    if (num_traverses == ++count_traverses) {
                        add_step(ms.get(), p_cur_start);
                        if (start_idx == r_idx)
                            add_step(ms.get(), p_cur_end);
                        break;
                    }

                    // Else, insert points regularly
                    add_step(ms.get(), p_cur_start);
                    add_step(ms.get(), p_cur_end);

                    reverse = !reverse;
                }
                add_step(ms.get(), end_p);
                mission.add_step(std::move(ms));
            }
        }
    }

    // Close path
    mission.add_step(std::make_unique<MissionPoint>(*mission.first_point()));
}
}


// Constructors
Planner::Planner(ros::NodeHandle &nh)
    : _nh{nh},
      _boundaryPub{_nh.advertise<visualization_msgs::Marker>("/" + OPTIONS::DRONE::NAME + "/planner/boundary", 1)},
      _obstaclesPub{_nh.advertise<visualization_msgs::MarkerArray>("/" + OPTIONS::DRONE::NAME + "/planner/obstacles", 1)},
      _convexPub{_nh.advertise<visualization_msgs::MarkerArray>("/" + OPTIONS::DRONE::NAME + "/planner/convex", 1)},
      _pathPub{_nh.advertise<visualization_msgs::Marker>("/" + OPTIONS::DRONE::NAME + "/planner/path", 1)},
      _poseSub{_nh.subscribe<geometry_msgs::PoseStamped>("/mavros/local_position/pose", 1, &Planner::_poseCb, this)} {}


// Functions
Mission
Planner::generate_mission(std::vector<P> &boundary,
                          const std::vector<std::vector<P>> &obstacles) const
{
    // Survey: https://www.mdpi.com/2504-446X/3/1/4
    // Convex decomposition (https://journals.sagepub.com/doi/full/10.1177/1729881419894787)
    ROS_INFO("Decomposing concave polygon into multiple convex polygons");
    std::vector<std::vector<P>> boundaries_convex;
    {
        // Convert multi-connected to single-connected
        multi2single(boundary, obstacles);

        // Decompose concave boundary
        decompose_concave(boundary, boundaries_convex);
    }

    // Path planning
    ROS_INFO("Planning mission");
    Mission mission(_nh, false);
    {
        // Wait until pose received
        {
            ros::Rate rate(OPTIONS::LINESCANNER::UPDATE_RATE);
            while (ros::ok() && (abs(_currentPose.x()) < 1e-10 || abs(_currentPose.y()) < 1e-10 || abs(_currentPose.z()) < 1e-10)) {
                ros::spinOnce();
                rate.sleep();
            }
        }

        plan_path(boundaries_convex, mission, _currentPose.p());
    }

    // If ROS not ready yet
    if (_boundaryPub.getNumSubscribers() == 0 || _pathPub.getNumSubscribers() == 0)
        ros::Duration(1).sleep();

    // Visualisation
    if (0 < _boundaryPub.getNumSubscribers())
        _boundaryPub.publish(path2marker(boundary, "planner_boundary", 1, 1, 1, 0.5));
    if (0 < _obstaclesPub.getNumSubscribers()) {
        visualization_msgs::MarkerArray ma;
        paths2markers(obstacles, ma, "planner_obstacles", 1, 0, 0, 0.25);
        _obstaclesPub.publish(ma);
    }
    if (0 < _convexPub.getNumSubscribers()) {
        visualization_msgs::MarkerArray ma;
        paths2markers(boundaries_convex, ma, "planner_convex", 0, 1, 0, 0.25);
        _convexPub.publish(ma);
    }
    if (0 < _pathPub.getNumSubscribers() && !mission.empty()) {
        std::vector<Point3d> path;
        mission.points(path);
        _pathPub.publish(path2marker(path, "planner_path", 1, 1, 0, 0.75));
    }

    return mission;
}
