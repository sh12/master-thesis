#include <linescanner.h>

// Private functions
namespace {
mt::Points
points2pointsmsg(std::vector<Linescanner::P> points)
{
    mt::Points ps;
    ps.points.resize(points.size());
    for (uint16_t i = 0; i < points.size(); ++i) {
        const Linescanner::P &p = points[i];
        mt::Point &p_msg = ps.points[i];
        p_msg.x = p.x();
        p_msg.y = p.y();
    }
    return ps;
}
}


// Constructors
Linescanner::Linescanner(ros::NodeHandle &nh)
    : _nh{nh},
      _scanSub{_nh.subscribe<sensor_msgs::LaserScan>("/" + OPTIONS::DRONE::NAME + "/scan", 1, &Linescanner::_scanCb, this)},
      _poseSub{_nh.subscribe<geometry_msgs::PoseStamped>("/mavros/local_position/pose", 1, &Linescanner::_poseCb, this)},
      _rvizPub{_nh.advertise<visualization_msgs::MarkerArray>("/" + OPTIONS::DRONE::NAME + "/scan_rviz", 1)},
      _pointsPub{_nh.advertise<mt::Points>("/" + OPTIONS::DRONE::NAME + "/local_points", 1)} {}


// Functions
void
Linescanner::run()
{
    ros::Rate rate(OPTIONS::LINESCANNER::UPDATE_RATE);

    // Wait until pose received
    ROS_INFO("Waiting for pose");
    while (ros::ok() && _wTc.z() < 1) {
        ros::spinOnce();
        rate.sleep();
    }
    Eigen::Affine2d wTp, wTc;
    _wTc.to2d(wTp);

    // Prepare SLOAM and useful variables
    SLOAM sloam(_nh, wTp);
    std::vector<P> points_world, points_drone;
    uint8_t iter = 0;
    bool end_sweep = false;

    while (ros::ok()) {
        if (!end_sweep)
            end_sweep = iter == OPTIONS::SLOAM::SWEEP_ITER - 1;

        // If new scan is received, convert and send it
        if (_newScanReceived) {
            _newScanReceived = false;

            // Convert scan to local points
            ls2worldxys(_currentScan, points_world);
            worldxys2xys(points_world, points_drone);

            // Publish lidar points
            if (0 < _pointsPub.getNumSubscribers())
                _pointsPub.publish(points2pointsmsg(points_drone));

            // If rviz subsribed
            if (0 < _rvizPub.getNumSubscribers()) {
                _rvizPub.publish(P::points2markers(points_world, 1. / OPTIONS::LINESCANNER::UPDATE_RATE));
            } else ROS_DEBUG("No subscribers!");

            // Process SLOAM
            if (1 < _wTc.z()) {
                _wTc.to2d(wTc);
                // Previous to current: pTc = pTw * wTc
                sloam.process(points_drone, wTp.inverse() * wTc, end_sweep);
                end_sweep = false;
            }

            wTp = wTc;
        }

        iter = (iter + 1) % OPTIONS::SLOAM::SWEEP_ITER;

        ros::spinOnce();
        rate.sleep();
    }
}

void
Linescanner::ls2worldxys(const sensor_msgs::LaserScan &ls,
                      std::vector<P> &xys) const
{
    xys.clear();
    double angle = ls.angle_min;
    const Eigen::Affine3d wTs = _wTc.affine() * OPTIONS::DRONE::droneTscanner;

    // Iterate ranges
    for (const float &range : ls.ranges) {
        angle += ls.angle_increment;
        if (OPTIONS::LINESCANNER::MIN_DIST_THRESH <= range && range != INFINITY)
            xys.emplace_back(Point3d(wTs * Point3d(cos(angle) * range, sin(angle) * range, 0)));
    }
}

void
Linescanner::worldxys2xys(const std::vector<P> &xys_world,
                            std::vector<P> &xys) const
{
    xys.resize(xys_world.size());
    Eigen::Affine2d wTc;
    _wTc.to2d(wTc);
    for (uint16_t i = 0; i < xys_world.size(); ++i)
        xys[i] = wTc.inverse() * xys_world[i];
}
