#include <line.h>

// Private functions
namespace {}


/* Abstract Base Class */
template<class P>
Line<P>::Line() {}

template <class P>
Line<P>::Line(const P &p,
              const P &q)
    : _p{p}, _q{q} {}


/* Line2d */
Line2d::Line2d() {}

Line2d::Line2d(const P &p,
               const P &q)
    : Line<P>{p, q} {}

// Functions
Line2d::P
Line2d::project(const P &p) const
{
    // Following https://stackoverflow.com/a/15187473/2546414
    const P q(_q - _p);
    const double t = ((p.x() - _p.x()) * q.x() + (p.y() - _p.y()) * q.y()) / (pow(q.x(), 2) + pow(q.y(), 2));
    return P(_p + t * q);
}

float
Line2d::dist_to(const P &p) const
{
    // Following https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    return abs((_q.x() - _p.x()) * (_p.y() - p.y()) - (_p.x() - p.x()) * (_q.y() - _p.y())) / sqrt(pow(_q.x() - _p.x(), 2) + pow(_q.y() - _p.y(), 2));
}


/* Line3d */
Line3d::Line3d() {}

Line3d::Line3d(const P &p,
               const P &q)
    : Line<P>{p, q} {}
