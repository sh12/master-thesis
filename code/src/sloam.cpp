#include <sloam.h>

#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>
#include <tf2_eigen/tf2_eigen.h>

#include <pcl/filters/statistical_outlier_removal.h>

// Private functions
namespace {
uint32_t
num_points(const std::vector<std::unique_ptr<Landmark>> &landmarks)
{
    uint32_t result = 0;
    for (const std::unique_ptr<Landmark> &lm : landmarks)
        result += lm->points.size();
    return result;
}

visualization_msgs::MarkerArray
landmarks2markers(const std::vector<std::unique_ptr<Landmark>> &landmarks,
                  const float &duration = 1. / OPTIONS::LINESCANNER::UPDATE_RATE,
                  const bool &include_points = true,
                  float r = -1,
                  float g = -1,
                  float b = -1,
                  const float &a = 1)
{
    // Random color
    auto randcol = []()->float{return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);};
    const bool randomize = r == -1 || g == -1 || b == -1;

    visualization_msgs::MarkerArray ma;
    for (const std::unique_ptr<Landmark> &lm : landmarks) {
        if (randomize) {
            r = randcol();
            g = randcol();
            b = randcol();
        }

        ma.markers.emplace_back(lm->circle.toMarker(ma.markers.size(), duration, r, g, b, a / 2));
        if (include_points) {
            for (const Landmark::P &p : lm->points)
                ma.markers.emplace_back(p.toMarker(ma.markers.size(), duration, r, g, b, a));
        }
    }
    return ma;
}

visualization_msgs::Marker
scanAreaMarker(const Pose &pose)
{
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = "sloam_scanarea";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::CYLINDER;
    marker.action = visualization_msgs::Marker::ADD;

    // Position
    marker.pose.position.x = pose.x();
    marker.pose.position.y = pose.y();
    marker.pose.position.z = pose.z();

    // Attitude
    marker.pose.orientation.w = pose.q0();
    marker.pose.orientation.x = pose.q1();
    marker.pose.orientation.y = pose.q2();
    marker.pose.orientation.z = pose.q3();

    // Size
    const float diameter = OPTIONS::LINESCANNER::RANGE * 2;
    marker.scale.x = diameter;
    marker.scale.y = diameter;
    marker.scale.z = 0.1;

    // Color
    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;
    marker.color.a = 0.25;

    marker.lifetime = ros::Duration(1. / OPTIONS::LINESCANNER::UPDATE_RATE);

    return marker;
}

void
createSizesVector(const std::vector<std::unique_ptr<Landmark>>& landmarks,
                  std::vector<uint32_t> &sizes)
{
    sizes.resize(landmarks.size());
    {
        sizes[0] = landmarks[0]->points.size();
        for (uint16_t i = 1; i < landmarks.size(); ++i)
            sizes[i] = sizes[i - 1] + landmarks[i]->points.size();
    }
}

visualization_msgs::Marker
association2marker(const std::pair<Landmark* const, std::vector<SLOAM::P>> &association,
                   const uint16_t id,
                   const float &duration = 1. / OPTIONS::LINESCANNER::UPDATE_RATE,
                   const float &r = 1.0,
                   const float &g = 1.0,
                   const float &b = 1.0,
                   const float &a = 1.0)
{
    const SLOAM::P &c = association.first->circle.center();
    const std::vector<SLOAM::P> &ps = association.second;

    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.ns = "association";
    marker.id = id;
    marker.type = visualization_msgs::Marker::LINE_STRIP;
    marker.action = visualization_msgs::Marker::ADD;

    // Position
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;

    // Size
    marker.scale.x = 0.01;

    // Attitude
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;

    // Color
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;
    marker.color.a = a;

    marker.lifetime = ros::Duration(duration);

    // Add point
    geometry_msgs::Point gp;
    gp.x = c.x();
    gp.y = c.y();
    gp.z = 0;
    marker.points.emplace_back(gp);
    for (const SLOAM::P &p : ps) {
        gp.x = p.x();
        gp.y = p.y();
        gp.z = 0;
        marker.points.emplace_back(gp);
        gp.x = c.x();
        gp.y = c.y();
        gp.z = 0;
        marker.points.emplace_back(gp);
    }

    return marker;
}

void
getAllPoints(const std::vector<std::unique_ptr<Landmark>> &landmarks,
             std::vector<Landmark::P> &ps)
{
    ps.clear();
    for (const std::unique_ptr<Landmark> &lm : landmarks)
        ps.insert(ps.end(), lm->points.begin(), lm->points.end());
}

visualization_msgs::MarkerArray
associations2markers(const std::map<Landmark* const, std::vector<SLOAM::P>> &associations,
                     const float &duration = 1. / OPTIONS::LINESCANNER::UPDATE_RATE,
                     const float &r = 1.0,
                     const float &g = 1.0,
                     const float &b = 1.0,
                     const float &a = 1.0)
{
    visualization_msgs::MarkerArray ma;
    for (const std::pair<Landmark* const, std::vector<SLOAM::P>> &as : associations)
        ma.markers.emplace_back(association2marker(as, ma.markers.size(), duration, r, g, b, a));
    return ma;
}

void
transformLandmarks(const Eigen::Affine2d &affine,
                   std::vector<std::unique_ptr<Landmark>> &landmarks)
{
    Eigen::Affine2d aff(affine);
    for (std::unique_ptr<Landmark> &lm : landmarks) {
        for (Landmark::P &p : lm->points)
            p = aff * p;
        lm->circle.center() = aff * lm->circle.center();
    }
}

void
createKdTree(const std::vector<std::unique_ptr<Landmark>> &landmarks,
             pcl::search::KdTree<SLOAM::pclP> &kdtree)
{
    SLOAM::pclC::Ptr cloud(new SLOAM::pclC(num_points(landmarks), 1));
    SLOAM::pclP *point;
    uint32_t i = 0;
    for (const std::unique_ptr<Landmark> &lm : landmarks) {
        for (const Landmark::P &p : lm->points) {
            point = &(*cloud)[i++];
            point->x = p.x();
            point->y = p.y();
            point->z = 0;
        }
    }
    kdtree.setInputCloud(cloud);
}

void
createKdTree(const std::vector<Landmark::P> &points,
             pcl::search::KdTree<SLOAM::pclP> &kdtree)
{
    SLOAM::pclC::Ptr cloud(new SLOAM::pclC(points.size(), 1));
    SLOAM::pclP *point;
    uint32_t i = 0;
    for (const Landmark::P &p : points) {
        point = &(*cloud)[i++];
        point->x = p.x();
        point->y = p.y();
        point->z = 0;
    }
    kdtree.setInputCloud(cloud);
}

void
associateLandmarks(std::vector<std::unique_ptr<Landmark>> &landmarks,
                   std::vector<Landmark::P> &points,
                   std::map<Landmark* const, std::vector<Landmark::P>> &associations,
                   const bool &remove_points = false)
{
    associations.clear();
    if (!landmarks.empty()) {
        // Helper lambda
        auto store_association = [&points, &associations, &remove_points](
                Landmark* const lm,
                uint16_t &i)->void{
            if (associations.find(lm) == associations.end())
                associations[lm] = std::vector<SLOAM::P>();
            associations[lm].emplace_back(points[i]);

            if (remove_points) {
                points[i--] = points.back();
                points.pop_back();
            }
        };

        // Helper variables
        Landmark *a_lm;

        switch (OPTIONS::SLOAM::ASSOCIATE_TYPE) {
        case OPTIONS::SLOAM::POINTS:
            {
                std::vector<uint32_t> sizes;
                std::unique_ptr<SLOAM::pclP> point = std::make_unique<SLOAM::pclP>();
                const uint8_t K = 1;
                std::vector<int> nn_idcs(K);
                std::vector<float> nn_dists(K);

                // Create lookup vector
                createSizesVector(landmarks, sizes);

                // Prepare kdtree for nearest neighbor search
                pcl::search::KdTree<SLOAM::pclP> kdtree(false);
                createKdTree(landmarks, kdtree);

                // Nearest neighbour search
                for (uint16_t i = 0; i < points.size(); ++i) {
                    const Landmark::P &p = points[i];

                    // Associate point point in previous sweep
                    point->x = p.x();
                    point->y = p.y();
                    point->z = 0;
                    kdtree.nearestKSearch(*point, K, nn_idcs, nn_dists);

                    // Find tree corresponding to point
                    uint16_t lm_idx = std::distance(sizes.begin(), std::lower_bound(sizes.begin(), sizes.end(), nn_idcs.front()));
                    a_lm = landmarks[lm_idx].get();

                    // Store associations
                    if (a_lm->circle.dist_to(p) <= OPTIONS::SLOAM::ASSOCIATE_DIST)
                        store_association(a_lm, i);
                }
            } break;

        case OPTIONS::SLOAM::CIRCLE:
            {
                float min_d, d;
                for (uint16_t i = 0; i < points.size(); ++i) {
                    const Landmark::P &p = points[i];

                    // Get smallest distance
                    min_d = FLT_MAX;
                    for (const std::unique_ptr<Landmark> &lm : landmarks) {
                        d = lm->circle.dist_to(p);
                        if (d < min_d) {
                            min_d = d;
                            a_lm = lm.get();
                        }
                    }

                    // If smallest distance is close enough
                    if (min_d <= OPTIONS::SLOAM::ASSOCIATE_DIST)
                        store_association(a_lm, i);
                }
            } break;

        default:
            throw std::invalid_argument("OPTIONS::SLOAM::ASSOCIATE_TYPE invalid type: " + std::to_string(OPTIONS::SLOAM::ASSOCIATE_TYPE));
            break;
        }
    }
}

void
fixAffine(Eigen::Affine2d &affine)
{
    const Eigen::Matrix2d &rot = affine.linear();
    const double c = (rot(0, 0) + rot(1, 1)) / 2;
    const double s = (-rot(0, 1) + rot(1, 0)) / 2;
    affine.linear() << c, -s, s, c;
}

void
updateLandmarks(std::vector<std::unique_ptr<Landmark>> &landmarks,
                const std::vector<SLOAM::P> &points,
                std::map<Landmark* const, std::vector<Landmark::P>> associations,
                const SLOAM::P &cur_pos = SLOAM::P(0, 0))
{
    // Create landmark and check validity
    auto is_valid = [&cur_pos](Landmark * const lm)->bool{
        const std::vector<SLOAM::P> &ps = lm->points;

        // Create circle
        lm->circle = C(ps);

        // If circle radius too small/big
        if (lm->circle.radius() < OPTIONS::SLOAM::CIRC_MIN_RADIUS || OPTIONS::SLOAM::CIRC_MAX_RADIUS < lm->circle.radius())
            return false;

        // If circle too far away
        if (OPTIONS::SLOAM::CIRC_MAX_OFFSET < lm->circle.center().dist_to(cur_pos))
            return false;

        float var = 0;
        for (const SLOAM::P &p : ps)
            var += pow(lm->circle.dist_to(p), 2);
        var /= ps.size();
        if (OPTIONS::SLOAM::CIRC_MAX_VAR < var)
            return false;

        return true;
    };

    // Update existing associated landmarks
    for (std::pair<Landmark* const, std::vector<Landmark::P>> &a : associations)
        a.first->circle = C(a.first->points);

    // If any points, try and create new landmarks
    if (!points.empty()) {
        // Prepare clustering
        pcl::search::KdTree<SLOAM::pclP>::Ptr kdtree(new pcl::search::KdTree<SLOAM::pclP>(false));
        createKdTree(points, *kdtree);

        // Radius outlier removal
        {
            // Find outliers
            SLOAM::pclC::ConstPtr cloud = kdtree->getInputCloud();
            SLOAM::pclC::Ptr cloud_filtered(new SLOAM::pclC());

            if (true) {
                std::vector<int> k_indices;
                std::vector<float> k_dists;
                pcl::PointIndices::Ptr outliers(new pcl::PointIndices());
                for (uint16_t i = 0; i < cloud->size(); ++i) {
                    kdtree->radiusSearch(i, OPTIONS::SLOAM::OUTLIER_RADIUS, k_indices, k_dists, OPTIONS::SLOAM::MIN_INLIERS);
                    if (k_indices.size() != OPTIONS::SLOAM::MIN_INLIERS)
                        outliers->indices.emplace_back(i);
                }

                // Do filtering
                pcl::ExtractIndices<SLOAM::pclP> extract;
                extract.setInputCloud(cloud);
                extract.setIndices(outliers);
                extract.setNegative(true);
                extract.filter(*cloud_filtered);
            } else {
                pcl::StatisticalOutlierRemoval<SLOAM::pclP> sor;
                sor.setInputCloud(cloud);
                sor.setMeanK(OPTIONS::SLOAM::MIN_INLIERS);
                sor.setStddevMulThresh(1.0);
                sor.filter(*cloud_filtered);
            }
            kdtree->setInputCloud(cloud_filtered);
        }

        // Do clustering
        {
            SLOAM::pclC::ConstPtr cloud = kdtree->getInputCloud();
            pcl::EuclideanClusterExtraction<SLOAM::pclP> ec;
            std::vector<pcl::PointIndices> idcs;
            ec.setClusterTolerance(OPTIONS::SLOAM::SEG_CUT_THRESH);
            ec.setMinClusterSize(OPTIONS::SLOAM::CIRC_MIN_NUM_PTS);
            ec.setSearchMethod(kdtree);
            ec.setInputCloud(kdtree->getInputCloud());
            ec.extract(idcs);

            for (const pcl::PointIndices &pi : idcs) {
                std::unique_ptr<Landmark> lm = std::make_unique<Landmark>();

                // Add points to new landmark
                for (const int &i : pi.indices) {
                    const SLOAM::pclP &p = (*cloud)[i];
                    lm->points.emplace_back(p.x, p.y);
                }

                // Add landmark if valid
                if (is_valid(lm.get()))
                    landmarks.emplace_back(std::move(lm));
            }
        }
    }
}

mt::Circles
landmarks2circlesmsg(const std::vector<std::unique_ptr<Landmark>> &landmarks)
{
    mt::Circles circles;
    circles.circles.resize(landmarks.size());
    for (uint16_t i = 0; i < landmarks.size(); ++i) {
        const C &c = landmarks[i]->circle;
        mt::Circle &circ = circles.circles[i];
        circ.x = c.center().x();
        circ.y = c.center().y();
        circ.r = c.radius();
    }
    return circles;
}
}


// Constructors
SLOAM::SLOAM(ros::NodeHandle &nh, const Eigen::Affine2d &wTc)
    : _nh{nh},
      _posePub{_nh.advertise<geometry_msgs::PoseStamped>("/" + OPTIONS::DRONE::NAME + "/sloam/pose", 1)},
      _scanAreaPub{_nh.advertise<visualization_msgs::Marker>("/" + OPTIONS::DRONE::NAME + "/sloam/scan_area", 1)},
      _linePub{_nh.advertise<visualization_msgs::Marker>("/" + OPTIONS::DRONE::NAME + "/sloam/line", 1)},
      _landmarksPub{_nh.advertise<visualization_msgs::MarkerArray>("/" + OPTIONS::DRONE::NAME + "/sloam/landmarks", 1)},
      _landmarksPrevPub{_nh.advertise<visualization_msgs::MarkerArray>("/" + OPTIONS::DRONE::NAME + "/sloam/landmarks_prev", 1)},
      _landmarksWorldCurPub{_nh.advertise<visualization_msgs::MarkerArray>("/" + OPTIONS::DRONE::NAME + "/sloam/landmarks_world_cur", 1)},
      _landmarksWorldPub{_nh.advertise<visualization_msgs::MarkerArray>("/" + OPTIONS::DRONE::NAME + "/sloam/landmarks_world", 1)},
      _associationsPub{_nh.advertise<visualization_msgs::MarkerArray>("/" + OPTIONS::DRONE::NAME + "/sloam/associations", 1)},
      _circlesPub{_nh.advertise<mt::Circles>("/" + OPTIONS::DRONE::NAME + "/sloam/circles", 1)},
      _line{Pose::create_line("sloam_line", 1, 0, 0)},
      _ceres{CeresOptimizer(_nh)}
{
    _wTc = wTc;
}


// Functions
void
SLOAM::process(std::vector<P> &scan,
               const Eigen::Affine2d &pTc,
               const bool &sweep_finish)
{
    const ros::Time t_start = ros::Time::now();

    // At beginning of sweep
    const bool first_sweep = _sweep_start;
    if (_sweep_start) {
        _sweep_start = false;
        _landmarks_cur.clear();
    }

    // Transform points
    _wTc = _wTc * pTc;
    fixAffine(_wTc);
    transformLandmarks(pTc.inverse(), _landmarks_cur);
    transformLandmarks(pTc.inverse(), _landmarks_prev);

    // Check if new points match previously scanned landmarks
    std::map<Landmark* const, std::vector<P>> associations;
    {
        associateLandmarks(_landmarks_cur, scan, associations, true);

        if (!first_sweep && 1 < associations.size()) {
            const Eigen::Affine2d pTc_ceres = _ceres.optimize_pose(associations, (1. / OPTIONS::LINESCANNER::UPDATE_RATE - (ros::Time::now() - t_start).toSec()) / (sweep_finish ? 2 : 1));
            _wTc = _wTc * pTc_ceres;
            fixAffine(_wTc);
            transformLandmarks(pTc_ceres.inverse(), _landmarks_cur);
            transformLandmarks(pTc_ceres.inverse(), _landmarks_prev);
        } else ROS_WARN("Not enough current associations: %ld < 2", associations.size());

        // Add new points to current sweep
        for (const std::pair<Landmark* const, std::vector<P>> &a : associations) {
            for (const Landmark::P &p : a.second)
                a.first->points.emplace_back(p);
        }
    }

    // Update all landmarks and create new from unused scan points
    updateLandmarks(_landmarks_cur, scan, associations);

    // If 2 or more landmarks in current and previous sweep, optimise pose
    std::vector<Landmark::P> points;
    if (1 < _landmarks_prev.size() && 1 < _landmarks_cur.size()) {
        // Find current sweep landmark correspondences to previous sweep
        getAllPoints(_landmarks_cur, points);
        associateLandmarks(_landmarks_prev, points, associations);

        // Visualise
        if (0 < _associationsPub.getNumSubscribers() && !associations.empty())
            _associationsPub.publish(associations2markers(associations, 1. / OPTIONS::LINESCANNER::UPDATE_RATE));

        // Optimise pose (transform applied to current sweep to match previous sweep)
        if (first_sweep && 1 < associations.size()) {
            const Eigen::Affine2d pTc_ceres = _ceres.optimize_pose(associations, (1. / OPTIONS::LINESCANNER::UPDATE_RATE - (ros::Time::now() - t_start).toSec()) / (sweep_finish ? 2 : 1));
            _wTc = _wTc * pTc_ceres;
            fixAffine(_wTc);
            transformLandmarks(pTc_ceres.inverse(), _landmarks_prev);
        }
    } else ROS_WARN("Not enough previous or current landmarks: %ld/%ld < 2", _landmarks_prev.size(), _landmarks_cur.size());

    // Publish landmarks as circles and rviz
    if (0 < _landmarksPub.getNumSubscribers())
        _landmarksPub.publish(landmarks2markers(_landmarks_cur, 1. / OPTIONS::LINESCANNER::UPDATE_RATE, true, 1, 0, 1, 0.5));

    // At end of sweep
    if (sweep_finish) {
        // Prepare for next sweep
        _sweep_start = true;

        // Move landmarks from current sweep to previous sweep
        _landmarks_prev.clear();
        for (std::unique_ptr<Landmark> &lm : _landmarks_cur)
            _landmarks_prev.emplace_back(std::make_unique<Landmark>(*lm));

        // Do mapping part
        {
            Eigen::Affine2d wcTwu = Eigen::Affine2d::Identity();

            // Transform landmarks from finished sweep into world coordinates
            transformLandmarks(_wTc, _landmarks_cur);
            getAllPoints(_landmarks_cur, points);

            // Associate these landmarks to world landmarks
            associateLandmarks(_landmarks_world, points, associations, true);

            if (1 < associations.size()) {
                // Optimize pose (transform applied to finished sweep to match world landmarks)
                wcTwu = _ceres.optimize_pose(associations, (1. / OPTIONS::LINESCANNER::UPDATE_RATE - (ros::Time::now() - t_start).toSec()) / 2);
                _wTc = wcTwu * _wTc;
                fixAffine(_wTc);
            }

            // Add new points to current sweep
            for (const std::pair<Landmark* const, std::vector<P>> &a : associations) {
                for (const Landmark::P &p : a.second)
                    a.first->points.emplace_back(wcTwu * p);
            }

            // Update world landmarks and create new from unused points
            updateLandmarks(_landmarks_world, points, associations, P(_wTc));

            // Publish world circles
            if (0 < _circlesPub.getNumSubscribers())
                _circlesPub.publish(landmarks2circlesmsg(_landmarks_world));

            // Visualise
            if (0 < _landmarksWorldCurPub.getNumSubscribers()) {
                transformLandmarks(wcTwu, _landmarks_cur);
                _landmarksWorldCurPub.publish(landmarks2markers(_landmarks_cur, OPTIONS::SLOAM::SWEEP_ITER / OPTIONS::LINESCANNER::UPDATE_RATE, false, 1, 0, 0, 0.5));
            }
            if (0 < _landmarksWorldPub.getNumSubscribers())
                _landmarksWorldPub.publish(landmarks2markers(_landmarks_world, 0, false, 0, 1, 0, 0.5));
        }
    }

    // Visualise
    const Pose pose = Pose(_wTc, OPTIONS::PLANNER::ALTITUDE);
    if (0 < _posePub.getNumSubscribers())
        _posePub.publish(pose.toMarker());
    if (0 < _scanAreaPub.getNumSubscribers())
        _scanAreaPub.publish(scanAreaMarker(pose));
    if (0 < _landmarksPrevPub.getNumSubscribers())
        _landmarksPrevPub.publish(landmarks2markers(_landmarks_prev, 1. / OPTIONS::LINESCANNER::UPDATE_RATE, true, 0, 0, 1, 0.5));
    if (0 < _linePub.getNumSubscribers()) {
        pose.update_line(_line);
        _linePub.publish(_line);
    }

    std::cout << ros::Time::now() - t_start << std::endl;
}
