#include <tracker.h>

// Private functions
namespace {}


// Constructors
Tracker::Tracker(ros::NodeHandle &nh)
    : _nh{nh},
      _poseSub{_nh.subscribe<geometry_msgs::PoseStamped>("/mavros/local_position/pose", 1, &Tracker::_poseCb, this)},
      _rvizPub{_nh.advertise<visualization_msgs::Marker>("/" + OPTIONS::DRONE::NAME + "/local_pose_trail", 1)} {}


// Functions
void
Tracker::run()
{
    ros::Rate rate(OPTIONS::TRACKER::UPDATE_RATE);
    visualization_msgs::Marker line = Pose::create_line("local_pose_trail");

    while (ros::ok()) {
        // If rviz subsribed
        if (0 < _rvizPub.getNumSubscribers()) {
            // Generate marker
            _currentPose.update_line(line);

            // Publish
            _rvizPub.publish(line);
        } else line.points.clear();

        ros::spinOnce();
        rate.sleep();
    }
}
