#include <missionstep.h>

// Private functions
namespace {}


/* MissionStep */
MissionStep::MissionStep(const float &max_duration)
    : _max_duration{max_duration} {}


/* MissionPoint */
MissionPoint::MissionPoint(const P &point,
                           const float &max_duration)
    : MSt{max_duration}, _p{point} {}

MissionPoint::MissionPoint(const Point2d &point, const float &z, const float &max_duration)
    : MP{P(point.x(), point.y(), z), max_duration} {}

MissionPoint::MissionPoint(const double &x,
                           const double &y,
                           const double &z,
                           const float &max_duration)
    : MP{P(x, y, z), max_duration} {}

MissionPoint::MissionPoint(const MissionPoint &mp)
    : MP{mp.p(), float(mp.max_duration().toSec())} {}

std::tuple<MissionPoint::P, MissionStep::RunStatus>
MissionPoint::run_step(const Pose &pose,
                       bool &force_next_point)
{
    // Initialize timer
    if (_begin.isZero()) {
        _begin = ros::Time::now();
        if (!_max_duration.isZero()) {
            ros::Time dl = _begin + _max_duration;
            _deadline = _deadline.isZero() ? dl : std::min(dl, _deadline);
        }
    }

    // Check if deadline reached
    if (!_deadline.isZero() && _deadline < ros::Time::now())
        return std::make_tuple(_p, OutOfTime);

    if (ros::Duration(0) < remaining_duration())
        std::cout << remaining_duration() << "\r" << std::flush;

    // Get status
    RunStatus status;
    if (force_next_point || _p.dist_to(pose) < OPTIONS::MISSION::DIST_VISITED) {
        force_next_point = false;
        status = Finished;
    }
    else status = InProgress;

    return std::make_tuple(_p, status);
}

void
MissionPoint::print(const uint8_t hierarchy) const
{
    std::cout << std::string(hierarchy, ' ') << "(" << _p.x() << ", " << _p.y() << ", " << _p.z() << ")";
    if (!_max_duration.isZero())
        std::cout << " - " << _max_duration << " [s]";
    std::cout << std::endl;
}

void
MissionPoint::reset()
{
    _begin = ros::Time();
}

double
MissionPoint::dist_to(const P &point) const
{
    return p().dist_to(point);
}

double
MissionPoint::dist_to(const MP &point) const
{
    return dist_to(point.p());
}

/* MissionSequence */
MissionSequence::MissionSequence(const float &max_duration,
                                 const uint8_t &num_rounds)
    : MSt{max_duration}, _num_rounds{num_rounds} {}

MissionStep::MP*
MissionSequence::first_point()
{
    const std::vector<std::unique_ptr<MSt>> *steps = &_steps;
    const MS *ms = dynamic_cast<const MS*>(steps->front().get());
    while (ms != nullptr) {
        steps = &ms->_steps;
        ms = dynamic_cast<const MS*>(steps->front().get());
    }
    return dynamic_cast<MP*>(steps->front().get());
}

const MissionStep::MP*
MissionSequence::first_point() const
{
    const std::vector<std::unique_ptr<MSt>> *steps = &_steps;
    const MS *ms = dynamic_cast<const MS*>(steps->front().get());
    while (ms != nullptr) {
        steps = &ms->_steps;
        ms = dynamic_cast<const MS*>(steps->front().get());
    }
    return dynamic_cast<const MP*>(steps->front().get());
}

MissionStep::MP*
MissionSequence::last_point()
{
    const std::vector<std::unique_ptr<MSt>> *steps = &_steps;
    const MS *ms = dynamic_cast<const MS*>(steps->back().get());
    while (ms != nullptr) {
        steps = &ms->_steps;
        ms = dynamic_cast<const MS*>(steps->back().get());
    }
    return dynamic_cast<MP*>(steps->back().get());
}

const MissionStep::MP*
MissionSequence::last_point() const
{
    const std::vector<std::unique_ptr<MSt>> *steps = &_steps;
    const MS *ms = dynamic_cast<const MS*>(steps->back().get());
    while (ms != nullptr) {
        steps = &ms->_steps;
        ms = dynamic_cast<const MS*>(steps->back().get());
    }
    return dynamic_cast<const MP*>(steps->back().get());
}

void
MissionSequence::points(std::vector<P> &points) const
{
    MS *ms;
    for (const std::unique_ptr<MSt> &step : _steps) {
        ms = dynamic_cast<MS*>(step.get());
        if (ms != nullptr)
            ms->points(points);
        else
            points.emplace_back(dynamic_cast<MP*>(step.get())->p());
    }
}

float MissionSequence::distance() const
{
    const MS *ms;
    float distance = 0;
    const P *prev = &first_point()->p();
    for (const std::unique_ptr<MSt> &step : _steps) {
        ms = dynamic_cast<const MS*>(step.get());
        if (ms != nullptr) {
            distance += prev->dist_to(ms->first_point()->p()) + ms->distance();
            prev = &ms->last_point()->p();
        } else {
            const P &cur = dynamic_cast<const MP*>(step.get())->p();
            distance += prev->dist_to(cur);
            prev = &cur;
        }
    }
    return distance;
}

std::tuple<MissionSequence::P, MissionStep::RunStatus>
MissionSequence::run_step(const Pose &pose,
                          bool &force_next_point)
{
    // Initialize timer
    if (_begin.isZero()) {
        _begin = ros::Time::now();
        if (!_max_duration.isZero()) {
            ros::Time dl = _begin + _max_duration;
            _deadline = _deadline.isZero() ? dl : std::min(dl, _deadline);
        }
    }

    P point;
    MissionStep::RunStatus status;
    MissionStep *cur_step;

    while (ros::ok()) {
        // Get current substep
        cur_step = _steps[_cur_step].get();

        // Set substep deadline to nearest
        if (!_deadline.isZero()) {
            ros::Time *dl = &cur_step->deadline();
            *dl = dl->isZero() ? _deadline : std::min(_deadline, *dl);
        }

        // Run substep
        std::tie(point, status) = cur_step->run_step(pose, force_next_point);

        // If didn't finish, continue as usual
        if (status != Finished)
            break;
        // Else, check if this Sequence is finished as well and repeat if needed
        else if (_steps.size() <= ++_cur_step) {
            if (++_cur_round < _num_rounds)
                reset();
            else break;
        }
    }

    // Return point and status
    return std::make_tuple(point, status);
}

void
MissionSequence::print(const uint8_t hierarchy) const
{
    std::string indent = std::string(hierarchy, ' ');
    std::cout << indent << "<" << std::to_string(hierarchy) << " steps=" << _steps.size() << " rounds=" << std::to_string(_num_rounds) << ">\n";
    for (const std::unique_ptr<MSt> &step : _steps)
        step->print(hierarchy + 1);
    std::cout << indent << "</" << std::to_string(hierarchy) << ">\n";
}

void
MissionSequence::reset()
{
    _begin = ros::Time();
    _cur_step = 0;

    // Reset all substeps
    for (const std::unique_ptr<MSt> &step : _steps)
        step->reset();
}

void
MissionSequence::add_step(std::unique_ptr<MSt> step)
{
    _steps.emplace_back(std::move(step));
}

void
MissionSequence::add_step(const uint16_t position,
                          std::unique_ptr<MSt> step)
{
    _steps.insert(_steps.begin() + position, std::move(step));
}
