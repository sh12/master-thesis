import numpy as np


def main():
    theta = np.pi / 18
    # theta = 0
    c = np.cos(theta)
    s = np.sin(theta)
    T = np.array([
        [c, 0, s, 0.08],
        [0, 1, 0, 0],
        [-s, 0, c, -0.08],
        [0, 0, 0, 1]
    ])
    p = np.array([0.1, 0, 0, 1])
    print(T @ p)


if __name__ == '__main__':
    main()
