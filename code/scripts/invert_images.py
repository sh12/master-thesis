import cv2
from pathlib import Path
import numpy as np


def main():
    paths = Path('/home/seh/ws/School/Master Thesis/docs/thesis/res/tests/global_polys').glob('*.png')
    for p in paths:
        img = cv2.imread(str(p))
        img = 255 - img
        img_thresh = 255 - cv2.inRange(img, (206, 206, 206), (208, 208, 208))
        img_thresh = 255 - cv2.dilate(img_thresh, np.ones((3, 3), np.uint8))
        # img[img_thresh] = (0, 0, 0)
        # cv2.imshow(str(p), img_thresh)
        p_inv = f'{str(p)[:-4]}-inv.png'
        # cv2.imwrite(p_inv, img_thresh)
        print(p)
        # img_thresh = cv2.thres


if __name__ == '__main__':
    main()
