#!/usr/bin/env python3

from pyutils.prints import *

import rospy
import rospkg
from gazebo_msgs.srv import SpawnModel, DeleteModel
from std_srvs.srv import Empty
from geometry_msgs.msg import Pose, Point, Quaternion

from pathlib import Path
import random
import numpy as np

from tqdm import tqdm

if __name__ == '__main__':
    model_name = 'simplified_wood'
    spawn_origin = (60, 0)
    spawn_max_dist = 50
    num_models = 70
    num_model_pairs = 0
    seed = 1
    min_dist = 1.5
    random.seed(seed)

    # ROS Service prep
    srv_spawn = '/gazebo/spawn_sdf_model'
    srv_del = '/gazebo/delete_model'
    srv_pause = '/gazebo/pause_physics'
    srv_unpause = '/gazebo/unpause_physics'

    L.info('Waiting for gazebo services')
    rospy.wait_for_service(srv_spawn)
    rospy.wait_for_service(srv_del)
    rospy.wait_for_service(srv_pause)
    rospy.wait_for_service(srv_unpause)

    L.info('Services up!')
    spawn_model = rospy.ServiceProxy(srv_spawn, SpawnModel)
    delete_model = rospy.ServiceProxy(srv_del, DeleteModel)
    pause = rospy.ServiceProxy(srv_pause, Empty)
    unpause = rospy.ServiceProxy(srv_unpause, Empty)

    L.info('Getting package path')
    rospack = rospkg.RosPack()
    path_pkg = Path(rospack.get_path('mt'))

    L.info('Loading .sdf')
    path_sdf = path_pkg / 'models' / model_name / 'model.sdf'
    with open(path_sdf, 'r') as f:
        model = f.read()

    L.info(yellow('Pausing physics'))
    pause()

    area = np.pi * spawn_max_dist ** 2

    L.info(f'Spawning {green(num_models)} models in {green(f"{area:.2f}")} m2')
    L.info(
        f'Density: {green(f"{num_models / area:.4f}")} models per m2 or avg {green(f"{np.sqrt(area / (num_models * np.pi)):.4f}")} m free space per model')
    quat = Quaternion(0, 0, 0, 1)

    xys = {(0, 0)}


    def valid(x, y):
        for px, py in xys:
            if np.sqrt((px - x) ** 2 + (py - y) ** 2) < min_dist:
                return False
        return True

    for i in tqdm(range(num_models)):
        name = f'{model_name}_{i}'

        while True:
            x = 25 + random.uniform(-15, 15)
            y = 0 + random.uniform(-15, 15)

            if valid(x, y):
                xys.add((x, y))
                break

        xyz = (x, y, 5)
        pose = Pose(Point(*xyz), quat)
        spawn_model(name, model, '', pose, 'world')

    if False:
        for i in tqdm(range(num_models)):
            name = f'{model_name}_{i}'

            while True:
                rand_d = spawn_max_dist * np.sqrt(random.uniform(0, 1))
                rand_theta = random.uniform(0, 2 * np.pi)
                x = spawn_origin[0] + np.cos(rand_theta) * rand_d
                y = spawn_origin[1] + np.sin(rand_theta) * rand_d

                if valid(x, y):
                    xys.add((x, y))
                    break

            xyz = (x, y, 5)
            pose = Pose(Point(*xyz), quat)
            spawn_model(name, model, '', pose, 'world')

    if False:
        for i in tqdm(range(num_model_pairs)):
            idx = 2 * i + num_models

            name = f'{model_name}_{idx}'
            x = 10 + i * 5
            y = 2.5
            xyz = (x, y, 5)
            pose = Pose(Point(*xyz), quat)
            spawn_model(name, model, '', pose, 'world')

            name = f'{model_name}_{idx + 1}'
            xyz = (x, -y, 5)
            pose = Pose(Point(*xyz), quat)
            spawn_model(name, model, '', pose, 'world')

    if False:
        L.info('Deleting')
        for i in tqdm(range(num_models + 2 * num_model_pairs)):
            name = f'{model_name}_{i}'
            delete_model(name)

    L.info(yellow('Unpausing physics'))
    unpause()
