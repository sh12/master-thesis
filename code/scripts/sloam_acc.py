#!/usr/bin/env python3

from typing import *

import rospy
from gazebo_msgs.msg import ModelStates
from geometry_msgs.msg import PoseStamped
from mt.msg import Circles

from matplotlib import pyplot as plt
import numpy as np


class Circle:
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r


cs_gz: List[Circle] = []
cs_sloam: List[Circle] = []
p_sloam: List[Tuple[float, float]] = []
p_mav: List[Tuple[float, float]] = []
p_gz: List[Tuple[float, float]] = []


def cbCircles(msg):
    cs_sloam.clear()
    for c in msg.circles:
        cs_sloam.append(Circle(c.x, c.y, c.r))


def cbGazebo(msg):
    p_gz.clear()
    cs_gz.clear()
    for i in range(len(msg.name)):
        pos = msg.pose[i].position
        if 'simplified_wood' in msg.name[i]:
            cs_gz.append(Circle(pos.x, pos.y, 0.5))
        elif 'mt_drone' in msg.name[i]:
            p_gz.append((pos.x, pos.y))


def cbPoseSloam(msg):
    p_sloam.clear()
    p_sloam.append((msg.pose.position.x, msg.pose.position.y))


def cbPoseMav(msg):
    p_mav.clear()
    p_mav.append((msg.pose.position.x, msg.pose.position.y))


def main():
    rospy.init_node('sloam_acc')
    rospy.Subscriber('/mt_drone/sloam/circles', Circles, cbCircles)
    rospy.Subscriber('/gazebo/model_states', ModelStates, cbGazebo)
    rospy.Subscriber('/mt_drone/sloam/pose', PoseStamped, cbPoseSloam)
    rospy.Subscriber('/mavros/local_position/pose', PoseStamped, cbPoseMav)
    rate = rospy.Rate(2)

    started = False
    ts = []
    pos_errs = []
    dbh_errs = []
    gz_size = []
    sloam_size = []
    poses_sloam = []
    poses_mav = []
    poses_gz = []

    fig, ax = plt.subplots(4, 1, figsize=(6, 8))
    while True:
        if not started:
            if not cs_sloam:
                rate.sleep()
                continue
            started = True
            start_time = rospy.Time.now()

        if not cs_sloam:
            break

        pos_err = []
        dbh_err = []
        for c in cs_sloam:
            d_min = 1e3
            for cc in cs_gz:
                d = np.sqrt((c.x - cc.x)**2 + (c.y - cc.y)**2)
                if d < d_min:
                    d_min = d
                    cc_min = cc
            pos_err.append(d_min)
            dbh_err.append(2 * abs(c.r - cc_min.r))

        t = rospy.Time.now() - start_time
        ts.append(t.secs + t.nsecs / 1e9)
        pos_errs.append(pos_err)
        dbh_errs.append(dbh_err)
        sloam_size.append(len(cs_sloam))
        gz_size.append(len(cs_gz))
        poses_sloam.append(p_sloam[-1])
        poses_mav.append(p_mav[-1])
        poses_gz.append(p_gz[-1])

        pos_means = [np.mean(errs) for errs in pos_errs]
        dbh_means = [np.mean(errs) for errs in dbh_errs]
        pos_sums = [np.sum(errs) for errs in pos_errs]
        dbh_sums = [np.sum(errs) for errs in dbh_errs]
        sloam_errs = [np.sqrt((poses_sloam[i][0] - poses_gz[i][0])**2 + (poses_sloam[i][1] - poses_gz[i][1])**2) for i in range(len(poses_sloam))]
        mav_errs = [np.sqrt((poses_mav[i][0] - poses_gz[i][0])**2 + (poses_mav[i][1] - poses_gz[i][1])**2) for i in range(len(poses_sloam))]

        for a in ax:
            a.clear()

        ax[0].plot(ts, pos_sums, label='Position error')
        ax[0].plot(ts, dbh_sums, label='DBH error')
        ax[1].plot(ts, pos_means, label='Position error')
        ax[1].plot(ts, dbh_means, label='DBH error')
        ax[2].plot(ts, sloam_errs, color='red', label='SLOAM')
        ax[2].plot(ts, mav_errs, color='purple', label='Pixhawk')
        ax[3].plot(ts, sloam_size, color='black', label='SLOAM')
        ax[3].plot(ts, gz_size, '--', color='gray', label='Ground truth')

        ax[0].set_xticklabels([])
        ax[1].set_xticklabels([])
        ax[2].set_xticklabels([])
        ax[3].set_xlabel(r'$t$ [s]')
        ax[0].set_ylabel(r'Error [m]')
        ax[1].set_ylabel(r'Error [m]')
        ax[2].set_ylabel(r'Error [m]')
        ax[3].set_ylabel(r'Count [#]')
        ax[0].legend(loc='upper left', frameon=True, facecolor='white', title='Accumulated landmark error')
        ax[1].legend(loc='upper left', frameon=True, facecolor='white', title='Mean landmark error')
        ax[2].legend(loc='upper left', frameon=True, facecolor='white', title='Drone position error')
        ax[3].legend(loc='upper left', frameon=True, facecolor='white', title='Number of landmarks')
        fig.savefig('sloam_acc.eps', bbox_inches='tight')

        rate.sleep()


if __name__ == '__main__':
    plt.style.use('seaborn')
    main()
