import numpy as np
from matplotlib import pyplot as plt

dhit = 1
dprox = 2
dinfl = 5


def theta(a, b):
    return np.arctan2(a[0] * b[1] - a[1] * b[0], a[0] * b[0] + a[1] * b[1])


def calc_vec(d_raw, a_raw, t_raw):
    d = np.array(d_raw)
    ac = np.array(a_raw[0])
    t = np.array(t_raw)

    u_d2t = t - d
    u_d2a = ac - d
    d_a = np.linalg.norm(u_d2a) - a_raw[1]
    if not dhit < d_a < dinfl or (d == t).all():
        return np.array([0, 0]), d_a, False

    u_d2a = u_d2a / np.linalg.norm(ac)
    u_d2t = u_d2t / np.linalg.norm(u_d2t)

    u_a2t = t - ac
    u_a2t = u_a2t / np.linalg.norm(u_a2t)

    theta_a = theta(u_d2a, u_a2t)
    if -np.pi / 2 < theta_a < 0:
        u_apre = np.array([u_d2a[1], -u_d2a[0]])
    elif 0 <= theta_a < np.pi / 2:
        u_apre = np.array([-u_d2a[1], u_d2a[0]])
    else:
        u_apre = u_d2t

    alpha = 0 if dinfl < d_a else ((d_a - dprox) / (dinfl - dprox)) ** 2
    a_smooth = alpha * u_d2t + (1 - alpha) * u_apre
    a_final = a_smooth
    if d_a < dprox:
        a_final -= (dprox - dhit) / (d_a - dhit) * u_d2a

    return a_final, d_a, True


def main():
    obs_a = ((6, -2), 0.5)
    obs_b = ((10, 2), 0.5)
    use_b = True
    target = (16, 0)

    xs = []
    ys = []
    us = []
    vs = []

    for x in np.linspace(0, 17, 35):
        for y in np.linspace(-8, 8, 33):
            vec_a, d_a, succ_a = calc_vec((x, y), obs_a, target)
            if not use_b and succ_a:
                xs.append(x)
                ys.append(y)
                us.append(vec_a[0])
                vs.append(vec_a[1])
            if use_b:
                vec_b, d_b, succ_b = calc_vec((x, y), obs_b, target)

                if dhit < d_a and dhit < d_b:
                    if succ_a or succ_b:
                        xs.append(x)
                        ys.append(y)
                    if succ_a:
                        if succ_b:
                            ratio = d_b / (d_a + d_b)
                            vec = vec_a * ratio + vec_b * (1 - ratio)
                            us.append(vec[0])
                            vs.append(vec[1])
                        else:
                            us.append(vec_a[0])
                            vs.append(vec_a[1])
                    elif succ_b:
                        us.append(vec_b[0])
                        vs.append(vec_b[1])

    fig, ax = plt.subplots(1, 1)
    ax.add_patch(plt.Circle(obs_a[0], obs_a[1] + dhit, color='red', linewidth=3, fill=False))
    ax.add_patch(plt.Circle(obs_a[0], obs_a[1] + dprox, color='orange', linewidth=3, fill=False))
    ax.add_patch(plt.Circle(obs_a[0], obs_a[1] + dinfl, color='blue', linewidth=3, fill=False))
    ax.add_patch(plt.Circle(obs_a[0], obs_a[1], color='brown', label=r'Obstacle $a$' if use_b else None))
    if use_b:
        ax.add_patch(plt.Circle(obs_b[0], obs_b[1] + dhit, color='red', linewidth=3, fill=False))
        ax.add_patch(plt.Circle(obs_b[0], obs_b[1] + dprox, color='orange', linewidth=3, fill=False))
        ax.add_patch(plt.Circle(obs_b[0], obs_b[1] + dinfl, color='blue', linewidth=3, fill=False))
        ax.add_patch(plt.Circle(obs_b[0], obs_b[1], color='purple', label=r'Obstacle $b$'))
    ax.quiver(xs, ys, us, vs, label='Avoidance direction', angles='xy')
    ax.plot(target[0], target[1], 'rX', ms=15, label='Target')
    ax.legend(loc='upper right', frameon=True, facecolor='white', title='Obstacle avoidance')
    ax.set_xlabel(r'$x$ [m]')
    ax.set_ylabel(r'$y$ [m]')
    plt.savefig('vectors.eps', bbox_inches='tight')
    plt.show()


if __name__ == '__main__':
    plt.style.use('seaborn')
    main()
