#!/usr/bin/env python3

from pyutils.prints import *

import rospy
from gazebo_msgs.msg import ModelState

import numpy as np


def main():
    rospy.init_node('rotate_drone')
    model_name = 'mt_drone'
    num_angles = 128
    revo_time = 100
    rate = rospy.Rate(num_angles / revo_time)
    angles = np.linspace(0, 2 * np.pi, num_angles + 1)[:-1]

    pub = rospy.Publisher('/gazebo/set_model_state', ModelState, queue_size=1)

    msg = ModelState()
    msg.model_name = model_name
    msg.pose.position.z = 0.127
    i = 0
    while True:
        theta = angles[i]
        msg.pose.orientation.w = np.cos(theta / 2)
        msg.pose.orientation.z = np.sin(theta / 2)
        pub.publish(msg)

        i = (i + 1) % num_angles
        rate.sleep()


if __name__ == '__main__':
    main()
