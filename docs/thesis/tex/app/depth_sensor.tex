\section{Depth sensor selection} \label{sec:a.depth}

    % Problem/Purpose
    A depth camera must be chosen that is able to detect trees around the drone.

    % Hypothesis


    % Materials
    The three following depth sensors will initially be considered:

    \begin{itemize}
        \item Hokuyo URG-04LX-UG01 \cite{urg}
        \item Realsense L515 \cite{l515}
        \item Texas Instruments IWR1443 \cite{iwr1443}
    \end{itemize}

    % Procedure
    Each depth camera's data sheet is initially inspected for relevant specifications. The depth camera will be selected by taking the following items into consideration:

    \begin{itemize}
        \item Footprint or resolution of the point cloud. Can the trees be distinguished from the point cloud? Are branches caught by the point cloud?
        \item \acrfull{fov} of the sensor. Wider \acrshort{fov} can detect more trees surrounding the drone.
        \item Range of the sensor. Longer sensor range can detect obstacles to avoid faster and also more trees.
        \item 2- or 3-dimensional data. 2-dimensional sensors must be moved/rotated to function as 3-dimensional sensor, e.g. by servo motor.
    \end{itemize}

    Additionally, since the sensor will be mounted on a drone, the following items will also be considered:

    \begin{itemize}
        \item Has mechanically moving components? Mechanical components are more likely to be damaged on rough landings.
        \item Mass and power consumption. Lower mass and power consumption extends the range and flight time of the drone.
    \end{itemize}

    Then each depth camera will be tested in two outdoor environments with trees, which can be seen in \cref{fig:tests_depth_env}. A point cloud will be extracted from each sensor and analysed.

    \begin{figure}[H]
        \centering
        \begin{subfigure}{0.49\linewidth}
            \includegraphics[width=\linewidth]{res/app/test_environment.jpg}
            \caption{First environment with many sticks on the trunk.}
            \label{fig:tests_depth_env1}
        \end{subfigure}
        \begin{subfigure}{0.49\linewidth}
            \includegraphics[width=\linewidth]{res/app/test_environment2.jpg}
            \caption{Second environment with bare trunks.}
            \label{fig:tests_depth_env2}
        \end{subfigure}
        \caption{Depth sensor test environments.}
        \label{fig:tests_depth_env}
    \end{figure}

    % Data/Results
    The relevant parameters can be seen in \cref{tab:tests_depth_params}.

    \begin{table}[H]
        \centering
        \caption{Relevant depth sensor parameters.}
        \label{tab:tests_depth_params}
        \begin{tabular}{l|ccc}
            Specification               & URG     & IWR1443           & L515            \\
            \hline
            Resolution$\u{\degree}$      & 0.352   & 15                & 0.068           \\
            \acrshort{fov}$\u{\degree}$  & 240     & $180 \times 180$  & $70 \times 55$  \\
            Range$\u{m}$                   & 5.6     & 9.01              & 0.25-9          \\
            Range resolution$\u{mm}$       & 30/3\%  & 44                & 9-14            \\
            2D/3D                       & 2D      & 3D                & 3D              \\
            Mechanical                  & Yes     & No                & No              \\
            Mass$\u{g}$                    & 160     & 32                & 100             \\
            Power consumption$\u{W}$       & 2.5     & -                 & -
        \end{tabular}
    \end{table}

    Since the L515 uses infrared light for range detection, the depth sensor of this \acrshort{lidar} works in an environment with at maximum $500\u{lx}$. This is about half the illuminance present on an overcast day \cite{wiki-lux}, which in the test environment gave little to no data. This depth sensor is thus omitted from the second step.

    The URG and IWR1443 scans can be seen in \cref{fig:tests_depth_scans}.

    \begin{figure}[H]
        \centering
        \begin{subfigure}{0.49\linewidth}
            \includegraphics[width=\linewidth]{res/app/urg_test.png}
            \caption{Hokuyo URG scan 1.}
            \label{fig:tests_depth_hokuyo-1}
        \end{subfigure}
        \begin{subfigure}{0.49\linewidth}
            \includegraphics[width=\linewidth]{res/app/urg_test2.png}
            \caption{Hokuyo URG scan 2.}
            \label{fig:tests_depth_hokuyo-2}
        \end{subfigure}

        \begin{subfigure}{0.49\linewidth}
            \includegraphics[width=\linewidth]{res/app/iwr1443_test_azi.png}
            \caption{IWR1443 azimuth view 1.}
            \label{fig:tests_depth_iwr-azi-1}
        \end{subfigure}
        \begin{subfigure}{0.49\linewidth}
            \includegraphics[width=\linewidth]{res/app/iwr1443_test_azi2.png}
            \caption{IWR1443 azimuth view 2.}
            \label{fig:tests_depth_iwr-azi-2}
        \end{subfigure}

        \begin{subfigure}{0.49\linewidth}
            \includegraphics[width=\linewidth]{res/app/iwr1443_test_elev.png}
            \caption{IWR1443 elevation view 1.}
            \label{fig:tests_depth_iwr-elev-1}
        \end{subfigure}
        \begin{subfigure}{0.49\linewidth}
            \includegraphics[width=\linewidth]{res/app/iwr1443_test_elev2.png}
            \caption{IWR1443 elevation view 2.}
            \label{fig:tests_depth_iwr-elev-2}
        \end{subfigure}
        \caption{Depth sensor test scans.}
        \label{fig:tests_depth_scans}
    \end{figure}

    % Analyse
    The Hokuyo has a much broader \acrshort{fov}. It does however only provide 2D data, and is mechanical, which is more prone to being damaged on rough landings, and has larger mass. In the IWR1443 scans, it is difficult to distinguish the trees from branches and leaves. Additionally, spurious data points were always present near origo, probably due to damage from previous usage. Conversely, trees can clearly be distinguished on the second Hokuyo scan. In the first Hokuyo scan, there is a lot of clutter from branches and leaves, but tree stems are still partly visible and can possible be distinguished with point cloud filtering methods. As such, the Hokuyo is used as the depth sensor.
